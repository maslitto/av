<?php

use App\Model\Page;

if (Schema::hasTable('pages')) {
    $pages = Page::where('parent_id',NULL)->get();
    foreach($pages as $page){
        Breadcrumbs::register($page->slug, function($breadcrumbs) use ($page)
        {
            $breadcrumbs->parent('home');
            $breadcrumbs->push($page->title, route($page->slug));
        });
    }
}

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Главная', route('home'));
});

// Home > Cart
Breadcrumbs::register('cart.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Корзина', route('cart.index'));
});
// Home > Order
Breadcrumbs::register('order.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Оформление заказа', route('order'));
});
// Home > Каталог
Breadcrumbs::register('catalog', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Каталог', route('catalog'));
});
// Home > Search
Breadcrumbs::register('search', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Результаты поиска', route('search'));
});

// Home > Каталог > [Product]
Breadcrumbs::register('product', function($breadcrumbs, $product)
{

    $breadcrumbs->parent('catalog');
    $breadcrumbs->push($product->title, route('product', $product->id));
});

// Home > Profile
Breadcrumbs::register('profile', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Личный кабинет', route('profile'));
});

// Home > [contacts]
Breadcrumbs::register('contacts', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Контакты', route('contacts'));
});

// Home > [oplata]
Breadcrumbs::register('oplata', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Доставка и оплата', route('oplata'));
});
Breadcrumbs::register('wishlist', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Избранное', route('wishlist'));
});

// Home > blog > [post]
Breadcrumbs::register('post', function($breadcrumbs, $post)
{
    //dd($blog);
    $breadcrumbs->parent('blog');
    //$breadcrumbs->push('Избранное', route('wishlist'));
    //dd($post);
    $breadcrumbs->push($post->title, route('post', $post->slug));
});