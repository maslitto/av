<?php

use App\Model\Page;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    if (Schema::hasTable('pages')) {
        $pages = Page::where('parent_id', NULL)->get();
        foreach ($pages as $page) {
            Route::get($page->slug, function () use ($page) {
                return view('content', ['page' => $page]);
            })->name($page->slug);
        }
    }
    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);
    Route::get('/glavnaya', function(){return redirect('');});
    Route::get('/parser', 'ParserController@parse');
    Route::resource('/cart','CartController');
    Route::get('/callback', 'CallbackController@callback');
    Route::get('/sitemap.xml', 'SitemapController@sitemap');

    /**Wishlist routes**/
    Route::get('/wishlist','WishlistController@index')->name('wishlist');
    Route::post('/wishlist','WishlistController@store')->name('wishlist.store');
    Route::post('/wishlist/destroy','WishlistController@destroy');
    Route::post('/wishlist/buyall','WishlistController@buyAll');

    /**Get single product*/
    Route::get('/products/{slug}', ['uses' => 'CatalogController@view', 'as' => 'product']);
    /**Get category*/
    Route::get('/catalog/{slug?}',['uses' => 'CatalogController@index', 'as' => 'catalog'] );
    Route::get('/tags/{slug}',['uses' => 'CatalogController@index', 'as' => 'tag'] );
    /*Order routes*/
    Route::get('order', 'OrderController@index')->name('order');
    Route::post('order/step3', 'OrderController@step3');
    Route::post('order/step4', 'OrderController@step4');
    Route::post('order/finish', 'OrderController@finish');
    /*Blog routes*/
    Route::get('/blog', 'BlogController@index')->name('blog');
    Route::get('/blog/{slug}', 'BlogController@view')->name('post');

    Route::get('/search','CatalogController@search')->name('search');
    Route::post('/filter', 'CatalogController@list');
    Route::get('/getbypid', 'CatalogController@getProductById');
    Route::get('register/confirm/{token}',[
        'as' => 'confirmation',
        'uses' => 'Auth\RegisterController@confirm'
    ]);

    /**Profile routes**/
    Route::get('/profile','ProfileController@index')->name('profile')->middleware('auth');
    Route::post('/profile','ProfileController@store')->middleware('auth');
    Route::post('/profile/changepassword','ProfileController@changePassword');

    /*Images routes*/
    Route::get('/images/{dateImg}/{filename}/{width}/{height}/{type?}/{anchor?}', 'ImageController@whResize');
    Route::get('/images/{dateImg}/{filename}/', 'ImageController@fullImage');

    /**Subscribe routes**/
    Route::post('/subscribe','SubscribeController@subscribe');
    Route::get('/subscribe/confirm/{token}','SubscribeController@confirm');
});
Route::get('api/orders/','ApiController@getOrders');
Route::post('api/users/','ApiController@getUsers');