@if(Agent::isMobile() || Agent::isTablet())
    <header class="header">
        <div class="header__line1">
            <a   class="collapsed callback" data-toggle="collapse" aria-expanded="false"  href="#callback">ЗАКАЗАТЬ ЗВОНОК</a>
            <div id="callback" class="toggle-cont collapse panel cl">
                <a href="#callback" data-toggle="collapse" aria-expanded="false" class="collapsed close"><img src="/template/img/icon-close.png" alt=""></a>
                <p class="form-title">Обратный звонок</p>
                <p>Оставьте Ваши контактные данные и наши менеджеры свяжутся с Вами</p>
                <form action="/" method="POST">
                    <div class="errors"></div>
                    <div class="form-group">
                        <label>Имя <span class="required">*</span></label>
                        <input type="text" class="form-control" placeholder="Имя" name="name">
                    </div>
                    <div class="form-group">
                        <label>Телефон <span class="required">*</span></label>
                        <input type="phone" class="form-control" placeholder="Телефон" name="phone">
                        <input type="hidden" name="robot" value="">
                    </div>
                    <div class="form-group">
                        <div class="checkbox checkbox-primary">
                            <input class="styled" id="terms" name="terms" checked value="1" type="checkbox">
                            <label for="terms">
                                Я согласен на <a href="#" class="pink" onclick="return false;"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Настоящим даю свое согласие ООО «Анжело Вани» на обработку любым допускаемым законом способом моих персональных данных, указанных в настоящей форме или сообщенных мною устно, в целях предоставления мне ответа на мое обращение и предложения мне услуг компании по телефону, в sms или по e-mail. Указанное согласие действует бессрочно с момента предоставления персональных данных и может быть отозвано мною путем подачи письменного заявления в компанию Анжело Вани. " rel="tooltip">обработку персональных данных</a>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Ждать звонка</button>
                    </div>
                </form>

            </div>
            <div class="phone"><i class="fa fa-phone" aria-hidden="true"></i> {{config('app.phone')}}</div>
            <div class="clearfix"></div>
        </div>
        <div class="header__line2">
            <div class="row">
                <div class="col-xs-3 text-center">
                    <ul class="nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bars fa-2x nav__icon" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu nav__menu">
                                <li><a href="/catalog/">ВСЕ ТОВАРЫ</a></li>
                                @include('/layouts/_menu',['id' => 2])
                                @include('/layouts/_menu',['id' => NULL])
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-6 text-center">
                    <img src="/template/img/logo.png" alt="" class="img-responsive header__logo">
                </div>
                <div class="col-xs-3">
                    <a  class="basket" href="/cart" aria-expanded="false"><img src="/template/img/icon-basket.png" alt="Корзина">
                        <span class="total basket__total">{{Cart::count()}}</span>
                    </a>
                </div>
            </div>
        </div>
    </header>
@else
    <header class="header">
        <div class="top-line">
            <div class="container">
                <div class="valign">
                    <span class="delivery-text">Наш адрес: {{config('app.address')}}</span>
                    <div style="position: relative;">
                        <a data-toggle="collapse" aria-expanded="false"  href="#callback" class="btn btn-primary collapsed">ЗАКАЗАТЬ ЗВОНОК</a>
                        <div id="callback" class="toggle-cont collapse panel js-callback">
                            <a href="#callback" data-toggle="collapse" aria-expanded="false" class="collapsed close"><img src="/template/img/icon-close.png" alt=""></a>
                            <p class="form-title">Обратный звонок</p>
                            <p>Оставьте Ваши контактные данные и наши менеджеры свяжутся с Вами</p>
                            <form action="/" method="POST" class="js-callback-form">
                                <div class="errors"></div>
                                <div class="form-group">
                                    <label>Имя <span class="required">*</span></label>
                                    <input type="text" class="form-control" placeholder="Имя" name="name">
                                </div>
                                <div class="form-group">
                                    <label>Телефон <span class="required">*</span></label>
                                    <input type="phone" class="form-control" placeholder="Телефон" name="phone">
                                    <input type="hidden" name="robot" value="">
                                </div>
                                <div class="form-group">
                                    <div class="checkbox checkbox-primary">
                                        <input class="styled" id="terms" name="terms" checked value="1" type="checkbox">
                                        <label for="terms">
                                            Я согласен на <a href="#" class="pink" onclick="return false;"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Настоящим даю свое согласие ООО «Анжело Вани» на обработку любым допускаемым законом способом моих персональных данных, указанных в настоящей форме или сообщенных мною устно, в целях предоставления мне ответа на мое обращение и предложения мне услуг компании по телефону, в sms или по e-mail. Указанное согласие действует бессрочно с момента предоставления персональных данных и может быть отозвано мною путем подачи письменного заявления в компанию Анжело Вани. " rel="tooltip">обработку персональных данных</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary js-callback-btn" type="submit">Ждать звонка</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="phone"><i class="fa fa-phone" aria-hidden="true"></i> {{config('app.phone')}}</div>
                    <span class="worktime">пн-пт с 9:00 до 18:00</span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="logo-line">
                <div class="row valign">
                    <div class="col-xs-4">
                        <form class="search-block" action="/search" method="GET">
                            <input type="text" placeholder="Поиск товара" name="q" class="form-control search-input">
                            <button><i class="fa fa-search"></i></button>
                            <ul id="search-results"></ul>
                        </form>
                    </div>
                    <div class="col-xs-4 text-center">
                        <a href="/"><img src="/template/img/logo.png" alt="Сумки и аксессуары Angelo Vani" class="logo"></a>
                    </div>
                    <div class="col-xs-4 valign">
                        @if (Auth::guest())
                            <div class="auth">
                                <div class="links-block">
                                    <a href="#login" class="collapsed" data-toggle="collapse" aria-expanded="false" >Вход</a>
                                    <span>/</span>
                                    <a href="#register" class="collapsed"  data-toggle="collapse" aria-expanded="false">Регистрация</a>
                                </div>
                                <!--div class="forms-block" id="forms-block"-->
                                <div id="login" class="toggle-cont collapse panel">
                                    <a href="#login" data-toggle="collapse" aria-expanded="false" class="collapsed close"><img src="/template/img/icon-close.png" alt=""></a>
                                    <p class="form-title">Вход</p>
                                    <p>Авторизуйтесь, если вы уже зарегистрированы на нашем сайте</p>
                                    <form action="/login" method="POST" class="login-form js-login-form">
                                        {!! csrf_field() !!}
                                        <div class="errors"></div>
                                        <div class="form-group">
                                            <label>E-mail <span class="required">*</span></label>
                                            <input type="email" class="form-control" placeholder="E-mail" name="email">
                                        </div>
                                        <div class="form-group">
                                            <label>Пароль <span class="required">*</span></label>
                                            <input type="password" class="form-control" placeholder="Пароль" name="password">
                                        </div>
                                        <p><a href="#forgot" data-toggle="collapse" aria-expanded="false" class="collapsed dashed-link">Забыли пароль?</a></p>
                                        <div class="form-group">
                                            <button class="btn btn-primary">ВОЙТИ</button>
                                        </div>
                                    </form>
                                    <hr>
                                    <p class="bottom-line"><a href="#register" data-toggle="collapse" aria-expanded="false" class="collapsed dashed-link">Еще не зарегистрированы?</a></p>
                                </div>
                                <div id="register" class="toggle-cont collapse panel">
                                    <a href="#register" data-toggle="collapse" aria-expanded="false" class="collapsed close"><img src="/template/img/icon-close.png" alt=""></a>
                                    <p class="form-title">Регистрация</p>
                                    <form action="/register" method="POST" class="register-form js-register-form">
                                        {!! csrf_field() !!}
                                        {{Form::hidden('robot')}}
                                        <div class="errors"></div>
                                        <div class="form-group">
                                            <label>E-mail <span class="required">*</span></label>
                                            <input type="email" class="form-control" placeholder="E-mail" name="email">
                                        </div>
                                        <div class="form-group">
                                            <label>Имя<span class="required">*</span></label>
                                            <input type="text" class="form-control" placeholder="Имя" name="name">
                                        </div>
                                        <div class="form-group">
                                            <label>Пароль <span class="required">*</span></label>
                                            <input type="password" class="form-control" placeholder="Пароль" name="password">
                                        </div>
                                        <div class="form-group">
                                            <label>Повторите пароль <span class="required">*</span></label>
                                            <input type="password" class="form-control" placeholder="Повторите пароль" name="password_confirmation">
                                        </div>
                                        <div class="form-group">
                                            <div class="radio radio-primary">
                                                <input id="buyer1" class="styled" checked="checked" name="opt" value="0" type="radio">
                                                <label for="buyer1">
                                                    Я розничный покупатель
                                                </label>
                                            </div>
                                            <div class="radio radio-primary">
                                                <input id="buyer2" class="styled"  name="opt" value="1" type="radio">
                                                <label for="buyer2">
                                                    Я оптовый покупатель
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary">
                                                <input class="styled" id="subscribe" name="subscribe"  checked value="1" type="checkbox">
                                                <label for="subscribe">
                                                    Получать ифнормацию о скидках и спецпредложениях
                                                </label>
                                            </div>
                                            <div class="checkbox checkbox-primary">
                                                <input class="styled" id="terms" name="terms"  checked value="1" type="checkbox">
                                                <label for="terms">
                                                    Я согласен на <a href="#" class="pink" onclick="return false;"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Настоящим даю свое согласие ООО «Анжело Вани» на обработку любым допускаемым законом способом моих персональных данных, указанных в настоящей форме или сообщенных мною устно, в целях предоставления мне ответа на мое обращение и предложения мне услуг компании по телефону, в sms или по e-mail. Указанное согласие действует бессрочно с момента предоставления персональных данных и может быть отозвано мною путем подачи письменного заявления в компанию Анжело Вани. " rel="tooltip">обработку персональных данных</a>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-default">ЗАРЕГИСТРИРОВАТЬСЯ</button>
                                        </div>
                                    </form>
                                    <hr>
                                    <p class="bottom-line"><span>Уже зарегистрированы? </span><a href="#login" data-toggle="collapse" aria-expanded="false" class="collapsed dashed-link">Войти</a></p>
                                </div>
                                <div id="forgot" class="toggle-cont collapse">
                                    <a href="#forgot" data-toggle="collapse" aria-expanded="false" class="collapsed close"><img src="/template/img/icon-close.png" alt=""></a>
                                    <p class="form-title">Восстановление пароля</p>
                                    <form action="forgot" role="form" method="POST" action="{{ url('/password/reset') }}" class="reset-form js-reset-form">
                                        {!! csrf_field() !!}
                                        <p>Введите ваш логин или E-mail, указанный при регистрации и мы вышлем ссылку на страницу смены пароля на ваш адрес.</p>
                                        <div class="errors"></div>
                                        <div class="form-group">
                                            <label>E-mail <span class="required">*</span></label>
                                            <input type="email" class="form-control" placeholder="E-mail" name="email">
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-default">ОТПРАВИТЬ</button>
                                        </div>
                                    </form>
                                    <hr>
                                    <p class="bottom-line"><span>Уже зарегистрированы? </span><a href="#login" data-toggle="collapse" aria-expanded="false" class="collapsed dashed-link">Войти</a></p>
                                </div>
                                <!--/div-->
                            </div>
                        @else
                            <ul >
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="/profile"><i class="fa fa-user"></i> Личный кабинет </a></li>

                                        <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-btn fa-sign-out"></i>Выход</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <form id="logout-form" action="/logout" method="POST" style="display: none;">
                                <input name="_token"  value="{!! csrf_token() !!}"  type="hidden">
                            </form>

                        @endif
                            {{--
                            <div class="wishlist-header">
                                <a href="/wishlist"><img src="/template/img/icon-wishlist.png" alt="Избранное">
                                    <span class="total basket__total">
                                        {{session()->has('wishlist') ? count(session()->get('wishlist')) : 0}}
                                    </span>
                                </a>
                            </div>
                            --}}
                            <div class="basket">
                                <a href="/cart/" {{--data-toggle="collapse" href="#basket-cont" aria-expanded="false" class="collapsed"--}}><img src="/template/img/icon-basket.png" alt="Корзина"> <span class="total basket__total">{{Cart::count()}}</span></a>
                            {{--
                                <ul id="basket-cont" class="toggle-cont collapse">
                                @foreach(Cart::content() as $item)
                                    <li class="basket-item">
                                        <div class="image">
                                            <a href="/products/{{$item->model->slug}}"><img src="/{{$item->model->resized('150x150')[0]}}" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="text">
                                            <p class="title"><a href="/products/{{$item->model->slug}}">{{$item->model->title}}</a></p>
                                            <p class="subtitle">Сумка женская</p>
                                            <p class="count">{{$item->qty}} x <span class="sum">{{$item->price}} <i class="fa fa-rouble"></i></span></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                @endforeach
                                <li class="button-item">
                                    <a href="/order" class="btn btn-default">ПЕРЕЙТИ К ОФОРМЛЕНИЮ</a>
                                    <a href="/cart" class="btn btn-primary">ПЕРЕЙТИ В КОРЗИНУ</a>
                                </li>
                                --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nav-line">
                <nav>
                    <div class="row">
                        <div class="col-xs-3">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">

                                    <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                        КАТАЛОГ ПРОДУКЦИИ
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/catalog/">ВСЕ ТОВАРЫ</a></li>
                                        @include('/layouts/_menu',['id' => 2])
                                    </ul>
                                </li>

                            </ul>
                        </div>
                        <div class="col-xs-9">
                            <ul class="nav navbar-nav">
                                @include('/layouts/_menu',['id' => NULL])
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
@endif
