@if($menu = App\Model\Page::where('parent_id',$id)->orderBy('lft','ASC')->get())
    @foreach($menu as $item)
        @if($id == NULL)
            <li><a href="{{$item->slug}}/" class="@if(isset($page)) @if($page->slug == $item->slug)active @endif @endif">{{$item->title}}</a></li>
        @else
            <li><a href="catalog/{{$item->slug}}/" class="@if(isset($page)) @if($page->slug == $item->slug)active @endif @endif">{{$item->title}}</a></li>
        @endif
    @endforeach
@endif