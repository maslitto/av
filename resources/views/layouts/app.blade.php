<!doctype html>
<html lang="ru">
<head>
    <base href="/"/>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="yandex-verification" content="0fb118b384b16a59"/>

    <!--meta name="viewport" content="width=1200"-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta id="token" name="token" class="js-token" content="{{ csrf_token() }}">
    <?php  if (!isset($page)) $page = App\Model\Page::find(1);  ?>

    <title>{{$page->metatitle}} | Сумки ANGELO VANI </title>

    <meta name="description" content="{{$page->description}}">
    <meta name="keywords" content="{{$page->keywords}}">

    <meta name="dc.subject" content="{{$page->metatitle}} ">
    <meta name="dc.description" content="{{$page->description}} ">
    <meta property="og:description" content="{{$page->description}} ">
    <meta name="dc.title" content="{{$page->metatitle}} ">
    <meta property="og:title" content="{{$page->metatitle}}">

    <base href="/">
    <!-- Latest compiled and minified CSS -->
    <!--link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/-->
    <!--link rel="stylesheet" type="text/css" href="/template/css/styles.scss"/-->
    <link href="/css/main.css" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
</head>
<body>
@include('/layouts/_header')

<div class="container">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block" style="margin-top:20px">
            <button type="button" class="close" data-dismiss="alert"><i class="fa fa fa-times"></i></button>
            <strong>Успешное завершение! </strong> {{ $message }}
        </div>
    @endif
    @yield('content')
</div>
@include('/layouts/_footer')
@include('/layouts/_modalproduct')
@section('js')

    <script src="/js/main.js"></script>
    @include('/layouts/_metrics')
@show
</body>
</html>
