<footer class="footer">
    <div class="container">
        <nav>
            <ul>
                @include('/layouts/_menu',['id' => NULL])
            </ul>
        </nav>

        <div class="row">
            <div class="col-md-3 footer__copyright">
                <div class="">
                    <p class="footer__copyright-text">
                        © 2007-{{date('Y')}} angelo-vani.ru <br>
                        Все права защищены.
                    </p>
                    <p class="footer__phone"><i class="fa fa-phone"></i> {{config('app.phone')}}</p>
                    <p class="footer__worktime">пн-пт с 9:00 до 18:00</p>
                </div>
            </div>
            <div class="col-md-6 footer__subscription" >
                <p>Подписывайтесь на наши акции и лучшие предложения</p>
                <form action="#" method="POST" class="subscription-form">
                    {!! csrf_field() !!}
                    {{Form::hidden('robot')}}
                    <input type="email" name="email" placeholder="Укажите Ваш e-mail" class="form-group">
                    <button type="submit"></button>
                </form>
                <p class="small"> Копирование любых материалов сайта строго запрещено<br/> и преследуется законом РФ.</p>
            </div>
            <div class="col-md-3 footer__payment">
                <div class="">
                    <p>Мы принимаем к оплате:</p>
                    <p>
                        <img src="/template/img/visa.png" alt="">
                        <img src="/template/img/mastercard.png" alt="">
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>