<!--MODAL-->
<div class="modal fade js-modalproduct" tabindex="-1" role="dialog" id="modalproduct">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Быстрый просмотр</h4>
            </div>
            <div class="modal-body js-modalproduct-body">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->