        <div class="single-product">
            {{--<div class="product-navigation hidden-xs">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="/products/{{$prev->slug}}" class="prev">
                            <p class="title hidden-xs">{{$prev->title}}</p>
                            <p class="subtitle hidden-xs">{{$prev->gender}}</p>
                        </a>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="/products/{{$next->slug}}" class="next">
                            <p class="title hidden-xs">{{$next->title}}</p>
                            <p class="subtitle hidden-xs">{{$next->vid}} {{$next->gender}}</p>
                        </a>
                    </div>
                </div>
            </div>--}}
            <div class="product-content">
                <div class="gallery">
                    <div class="main-image">
                        @foreach($product->resized('800x800') as $image)
                            <div class="slide text-center">
                                <img src="{{$image}}" alt="{{$product->title}}" class="img-responsive js-lens" data-big="{{$image}}" data-big2x="{{$image}}" >
                            </div>
                        @endforeach
                    </div>
                    {{--
                    <div class="thumbs text-center">
                        @foreach($product->resized('300x300') as $image)
                            <div class="slide text-center">
                                <img src="{{$image}}" alt="{{$product->title}}" class="img-responsive">
                            </div>
                        @endforeach
                    </div>
                    --}}
                </div>
                <div class="content">
                    <div class="text-block">
                        <h1>{{$product->vidnomenklature =='Сумки' ? 'Сумка ' : ''}} {{$product->gender}} "{{$product->vid}}" артикул {{$product->sku}}</h1>
                        <p class="stock">Наличие :
                            @if($product->stock > 0)
                                <strong class="text-success"><i class="fa fa-check"></i> Есть в наличии</strong>
                            @else
                                <strong class="text-danger"><i class="fa fa-close"></i> Нет в наличии</strong>
                            @endif
                        </p>
                        <p class="collection">Коллекция : <strong> {{$product->collection}}</strong></p>
                        <p class="brand">Бренд : <strong> Angelo Vani</strong></p>
                        @if(!empty($product->color))
                            <p class="brand">Цвет : <strong> {{$product->color}}</strong></p>
                        @endif  
                        
                        <div class="row price-block">
                            <div class="col-sm-3 col-xs-6">
                                <p>Цена (опт.)</p>
                                <p class="price">
                                {{--@if($product->price_sale>0)
                                    {{$product->price_sale}}
                                @else
                                    {{$product->price_opt}}
                                @endif
                                <i class="fa fa-rouble"></i>
                                --}}
                                    Цена скрыта
                                </p>
                            </div>
                            @if($product->old_price>0)
                                <div class="col-sm-3 col-xs-6">
                                    <p>Старая цена</p>
                                    <p class="old-price">{{ $product->old_price }}<i class="fa fa-rouble"></i></p>
                                </div>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        <div class="row button-block buy-form js-cart-add-form"  data-pid="{{$product->id}}">
                            <form method="POST" class="form js-form" action="{{route("cart.store")}}">
                                {!! csrf_field() !!}
                                <input type="hidden" value="{{$product->id}}" name="pid">
                                <div class="col-sm-12" style="margin-bottom: 15px;">
                                    <label class="form__number-input-b form__number-input-b--small form__b--one">
                                        <input name="count"
                                               type="number"
                                               step="1"
                                               min="1"
                                               value="1"
                                               data-id="{{$product->id}}"
                                               class="form__number-input js-form__number js-change-sum-number">
                                        <span class="form__number-input-minus js-form__number-minus">−</span>
                                        <span class="form__number-input-plus js-form__number-plus">+</span>
                                    </label>
                                </div>

                            <div class="col-sm-6 button-block__btn">
                                <button class="btn btn-default btn-buy js-cart-add" type="submit">добавить в корзину</button>
                            </div>
                            </form>
                            <div class="col-sm-6 button-block__btn">
                                <a class="btn btn-primary buy-1-click" href="#buy1click{{$product->id}}" data-toggle="collapse" aria-expanded="false">Купить в 1 клик</a>
                                <div id="buy1click{{$product->id}}" class="toggle-cont panel collapse buy1click" aria-expanded="false">
                                    <a href="#buy1click{{$product->id}}" data-toggle="collapse" aria-expanded="false" class="collapsed close "><img src="/template/img/icon-close.png" alt=""></a>
                                    <p class="form-title">Купить в 1 клик</p>
                                    <p>Оставьте Ваши контактные данные и наши менеджеры свяжутся с Вами</p>
                                    <form action="/" method="POST">
                                        <div class="form-group">
                                            <label>Имя <span class="required">*</span></label>
                                            <input class="form-control" placeholder="Имя" name="name" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label>Телефон <span class="required">*</span></label>
                                            <input class="form-control" placeholder="Телефон" name="phone" type="phone">
                                            <input name="robot" value="fuck-u-robots" type="hidden">
                                        </div>
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary">
                                                <input class="styled" id="terms" name="terms" checked value="1" type="checkbox">
                                                <label for="terms">
                                                    Я согласен на <a href="#" class="pink" onclick="return false;"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Настоящим даю свое согласие ООО «Анжело Вани» на обработку любым допускаемым законом способом моих персональных данных, указанных в настоящей форме или сообщенных мною устно, в целях предоставления мне ответа на мое обращение и предложения мне услуг компании по телефону, в sms или по e-mail. Указанное согласие действует бессрочно с момента предоставления персональных данных и может быть отозвано мною путем подачи письменного заявления в компанию Анжело Вани. " rel="tooltip">обработку персональных данных</a>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="submit">Купить!</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item active">
                                <a class="nav-link" data-toggle="tab" href="#panel1{{$product->id}}" role="tab">Описание</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel2{{$product->id}}" role="tab">Характеристики</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel3{{$product->id}}" role="tab">Доставка</a>
                            </li>
                        </ul>
                        <!-- Tab panels -->
                        <div class="tab-content">
                            <!--Panel 1-->
                            <div class="tab-pane fade in active" id="panel1{{$product->id}}" role="tabpanel">
                                {{$product->description}}  
                            </div>
                            <!--/.Panel 1-->
                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel2{{$product->id}}" role="tabpanel">
                                <p class="size">Вид номенклатуры: <strong> {{$product->vidnomenklature}}</strong></p> 
                                <p class="size">Вид: <strong> {{$product->vid}}</strong></p> 
                                <p class="size">Пол: <strong> {{$product->gender}}</strong></p> 
                                <p class="size">Сезон: <strong> {{$product->season}}</strong></p> 
                                <p class="size">Материал: <strong> {{$product->material}}</strong></p> 
                                <p class="size">Размеры (ВхШхГ) см.: <strong> {{$product->height}}x{{$product->width}}x{{$product->length}}</strong></p> 
                                
                            </div>
                            <!--/.Panel 2-->
                            <!--Panel 3-->
                            <div class="tab-pane fade" id="panel3{{$product->id}}" role="tabpanel">
                            <p>Доступна доставка в регионы.</p>
                            </div>
                            <!--/.Panel 3-->
                        </div>
                    </div>
                    
                    <div class="share-block">
                        <a href="#" class="wishlist js-wishlist   @if(session()->has('wishlist')) @if(in_array($product->id, session()->get('wishlist'))) active @endif @endif"  data-pid="{{$product->id}}"> В ИЗБРАННОЕ</a>
                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="//yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,viber,whatsapp,telegram"></div>
                    </div>
                </div>
            </div>
        </div>