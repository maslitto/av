    <!--BREADCRUMBS-->
    @if((strpos(Route::currentRouteName(), 'admin') !== false)) 
        @if ($breadcrumbs)
        <ul class="breadcrumb">
            @foreach ($breadcrumbs as $breadcrumb)
                @if (!$breadcrumb->last)
                    <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                @else
                    <li class="active">{{ $breadcrumb->title }}</li>
                @endif
            @endforeach
        </ul>
        @endif
    @else    
        <div class="breadcrumb">
            <div class="row">
                <div class="hidden-xs col-sm-6">
                    @if ($breadcrumbs)
                        <ol class="">
                            @foreach ($breadcrumbs as $breadcrumb)
                                @if (!$breadcrumb->last)
                                    <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                                @else
                                    <li class="active">{{ $breadcrumb->title }}</li>
                                @endif
                            @endforeach
                        </ol>
                    @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                     <ol class="text-right">
                        <li class=""><a href="{{ URL::previous() }}">‹ назад</a></li>
                    </ol>
                </div>
            </div>
        </div>
  
    @endif
    
