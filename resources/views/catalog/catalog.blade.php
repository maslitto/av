@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists() !!}
    <!--CATALOG-->
        <div class="catalog">
            @if(Agent::isMobile())
                <div class="catalog-header">
                    <img src="/template/img/catalog-header-mob.jpg" alt="" class="catalog-header__img">
                    <p class="catalog-header__catalog">КАТАЛОГ ПРОДУКЦИИ</p>
                    <h1>{{$page->title}}</h1>
                    <p class="catalog-header__description">
                        @if(!empty($page->description))
                            {{$page->description}}
                        @else
                            {{$page->metadescription}}
                        @endif
                    </p>
                </div>

            @else
                <div class="catalog-header">
                    <p>КАТАЛОГ ПРОДУКЦИИ</p>
                    <h1>{{$page->title}}</h1>
                    <p class="catalog-header__description">
                        @if(!empty($page->description))
                            {{$page->description}}
                        @else
                            {{$page->metadescription}}
                        @endif
                    </p>
                </div>
            @endif
            <section class="catalog-content">
                <div class="row">
                    @if(Agent::isMobile())
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-primary catalog__show-filters js-catalog-show-filters">показать фильтры</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-primary catalog__show-sorting js-catalog-show-sorting">Сортировать по</a>
                        </div>
                        <div class="clearfix"></div>
                    @endif

                    <div class="col-sm-3 hidden-xs js-catalog-filter catalog__filter">
                        <a href="#" class="visible-xs btn js-close-catalog-filter catalog-filter__close">Скрыть фильтры</a>
                        <form id="filterForm" class="js-catalog-filter-form" action="" method="GET" style="position:relative">
                            {{Form::hidden('per_page', Request::has('per_page') == null ? '24' :Request::get('per_page'))}}
                            {{Form::hidden('sortby',Request::get('sortby'))}}
                            {{Form::hidden('sortdir',Request::get('sortdir'))}}
                            {{Form::hidden('view', Request::has('view') == null ? 'block' :Request::get('view'))}}
                            <button class="btn popup-show" type="submit">Показать результаты ( <span class="count"> </span> )
                            </button>
                            <div class="panel-group" id="catalog-filter">
                                <!-- Для кого -->
                                @if(count($filters['genders'])>0)
                                    <div class="panel">
                                        <a class="panel-heading collapsed" data-toggle="collapse" href=".genders" aria-expanded="true">Для кого</a>
                                        <div class="panel-collapse collapse in genders" aria-expanded="true">
                                            <div class="panel-body">
                                                @foreach($filters['genders'] as $gender)
                                                    <div class="checkbox checkbox-primary">
                                                    {{Form::checkbox('lists[gender][]',$gender,!empty(Request::get('lists[gender]'))? in_array($gender,Request::get('lists[gender]')) : false,['class' => 'styled', 'id' => 'gender'.$gender])}}
                                                        <label for="{{'gender'.$gender}}">
                                                            {{ $gender }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <!-- По сезону -->
                                @if(count($filters['seasons'])>0)
                                    <div class="panel">
                                        <a class="panel-heading collapsed" data-toggle="collapse" href=".seasons" aria-expanded="true">Сезон</a>
                                        <div class="panel-collapse collapse in seasons" aria-expanded="true">
                                            <div class="panel-body">
                                                @foreach($filters['seasons'] as $season)
                                                    <div class="checkbox checkbox-primary">
                                                        {{Form::checkbox('lists[season][]',$season,!empty(Request::get('lists[season]'))? in_array($season,Request::get('lists[season]')) : false,['class' => 'styled', 'id' => 'season'.$season])}}
                                                        <label for="{{'season'.$season}}">
                                                            {{ $season }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <!-- По коллекции -->
                                @if(count($filters['collections'])>0)
                                    <div class="panel">
                                        <a class="panel-heading collapsed" data-toggle="collapse" href=".collections" aria-expanded="true">Коллекция</a>
                                        <div class="panel-collapse collapse in collections" aria-expanded="true">
                                            <div class="panel-body">
                                                @foreach($filters['collections'] as $collection)
                                                    <div class="checkbox checkbox-primary">
                                                    {{Form::checkbox('lists[collection][]',$collection,!empty(Request::get('lists[collection]'))? in_array($collection,Request::get('lists[collection]')) : false,['class' => 'styled', 'id' => 'collection'.$collection])}}
                                                        <label for="{{'collection'.$collection}}">
                                                            {{ $collection }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <!-- По виду номенклатуры -->
                                @if(count($filters['vidnomenklatures'])>0)
                                    <div class="panel">
                                        <a class="panel-heading collapsed" data-toggle="collapse" href=".vidnomenklatures" aria-expanded="true">Вид товара</a>
                                        <div class="panel-collapse collapse in vidnomenklatures" aria-expanded="true">
                                            <div class="panel-body">
                                                @foreach($filters['vidnomenklatures'] as $vidnomenklature)
                                                    <div class="checkbox checkbox-primary">
                                                    {{Form::checkbox('lists[vidnomenklature][]',$vidnomenklature,!empty(Request::get('lists[vidnomenklature]'))? in_array($vidnomenklature,Request::get('lists[vidnomenklature]')) : false,['class' => 'styled', 'id' => 'vidnomenklature'.$vidnomenklature])}}
                                                        <label for="{{'vidnomenklature'.$vidnomenklature}}">
                                                            {{ $vidnomenklature }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <!-- Слайдер цена -->
                                <div class="panel">
                                    <a class="panel-heading collapsed" data-toggle="collapse" href=".price" aria-expanded="false">Цена</a>
                                    <div id="collapse6" class="panel-collapse collapse in price" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="left">
                                                <input type="text" name="priceFrom" id="from" class="styled" placeholder="от" value="{{Request::has('priceFrom') ? Request::get('priceFrom') : $filters['priceMin']}}"/>
                                                <div class="minus"></div>
                                            </div>
                                            <div class="right">
                                                <input type="text" name="priceTo" id="to" class="styled" placeholder="до" value="{{Request::has('priceTo') ? Request::get('priceTo') : $filters['priceMax']}}">
                                            </div>
                                            <div class="clearfix"></div>
                                            <input class="styled" id="price-slider" type="text" data-slider-step="100"
                                                   data-slider-min="{{$filters['priceMin']}}"
                                                   data-slider-max="{{$filters['priceMax']}}"
                                                   data-slider-value="[{{Request::has('priceFrom') ? Request::get('priceFrom') : $filters['priceMin']}},{{Request::has('priceTo') ? Request::get('priceTo') : $filters['priceMax']}}]">

                                        </div>
                                    </div>
                                </div>
                                <!-- По материалу -->
                                @if(count($filters['materials'])>0)
                                    <div class="panel">
                                        <a class="panel-heading collapsed" data-toggle="collapse" href=".materials" aria-expanded="true">Материал</a>
                                        <div class="panel-collapse collapse in materials" aria-expanded="true">
                                            <div class="panel-body">
                                                @foreach($filters['materials'] as $material)
                                                    <div class="checkbox checkbox-primary">
                                                    {{Form::checkbox('lists[material][]',$material,!empty(Request::get('lists[material]'))? in_array($material,Request::get('lists[material]')) : false,['class' => 'styled', 'id' => 'material'.$material])}}
                                                        <label for="{{'material'.$material}}">
                                                            {{ $material }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <!-- По Виду -->
                                @if(count($filters['vids'])>0)
                                    <div class="panel">
                                        <a class="panel-heading collapsed" data-toggle="collapse" href=".vids" aria-expanded="true">Вид</a>
                                        <div class="panel-collapse collapse in vids" aria-expanded="true">
                                            <div class="panel-body">
                                                @foreach($filters['vids'] as $vid)
                                                    <div class="checkbox checkbox-primary">
                                                    {{Form::checkbox('lists[vid][]',$vid,!empty(Request::get('lists[vid]'))? in_array($vid,Request::get('lists[vid]')) : false,['class' => 'styled', 'id' => 'vid'.$vid])}}
                                                        <label for="{{'vid'.$vid}}">
                                                            {{ $vid }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif




                                <!-- По размеру -->
                                <!--div class="panel">
                                    <a class="panel-heading collapsed" data-toggle="collapse" href=".sizes" aria-expanded="true">Размер</a>
                                    <div class="panel-collapse collapse in sizes" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="checkbox checkbox-primary">
                                                <input class="styled" id="size1" name="sizes[]" value="1" type="checkbox">
                                                <label for="size1">
                                                    Большая <span class="count">(<span>11</span>)</span>
                                                </label>
                                            </div>
                                            <div class="checkbox checkbox-primary">
                                                <input class="styled" id="size2" name="sizes[]" value="2" type="checkbox">
                                                <label for="size2">
                                                    Среднаяя  <span class="count">(<span>15</span>)</span>
                                                </label>
                                            </div>
                                            <div class="checkbox checkbox-primary">
                                                <input class="styled" id="size3" name="sizes[]" value="3" type="checkbox">
                                                <label for="size3">
                                                    Малая  <span class="count">(<span>19</span>)</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div-->
                                <!-- По цвету -->
                                @if(count($filters['colors'])>0)
                                    <div class="panel">
                                        <a class="panel-heading collapsed" data-toggle="collapse" href=".colors" aria-expanded="true">Цвет</a>
                                        <div class="panel-collapse collapse in colors" aria-expanded="true">
                                            <div class="panel-body">
                                                <div class="panel-body">
                                                    @foreach($filters['colors'] as $color)
                                                        <div class="checkbox checkbox-primary">
                                                        {{Form::checkbox('lists[color][]',$color,!empty(Request::get('lists[color]'))? in_array($color,Request::get('lists[color]')) : false,['class' => 'styled', 'id' => 'color'.$color])}}
                                                            <label for="{{'color'.$color}}">
                                                                {{ $color }}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <!-- По наличию -->
                                <div class="panel">
                                    <a class="panel-heading collapsed" data-toggle="collapse" href=".stock" aria-expanded="true">Наличие</a>
                                    <div class="panel-collapse collapse in stock" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="panel-body">
                                                <div class="radio radio-primary">
                                                    {{Form::radio('stock','yes',Request::get('stock')=='no'? false : true,['id' => 'stock1','class' => 'styled'])}}
                                                    <label for="stock1">
                                                        В наличии
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    {{Form::radio('stock','no',Request::get('stock')=='no'? true : false,['id' => 'stock2','class' => 'styled'])}}
                                                    <label for="stock2">
                                                        Под заказ
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Подобрать -->
                                <div class="panel">
                                    <div class="panel-body">
                                        <div class="panel-body">
                                            <p class="text-center">Мы подобрали для вас <span class="count">{{$total}}</span> моделей</p>
                                            <p class="text-center"><button class="btn btn-primary">ПОКАЗАТЬ</button></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-9">

                        <div class="tags catalog__tags tags--pink">
                            @foreach($tags1 as $tag)
                                <a href="/tags/{{$tag->slug}}" class="tag tag--small {{$tag->id == $page->id ? 'tag--active' :''}}">#{{$tag->title}}</a>
                            @endforeach
                        </div>
                        <div class="tags catalog__tags tags--pink">
                            @foreach($tags2 as $tag)
                                <a href="/tags/{{$tag->slug}}" class="tag tag--small">#{{$tag->title}}</a>
                            @endforeach
                        </div>
                        <nav class="catalog-settings js-catalog-sorting hidden-xs">
                            <ul class="nav navbar-nav valign">
                                <li class="dropdown hidden-xs">
                                    Показывать по :
                                    <a  href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        {{Request::has('per_page') ? Request::get('per_page') : \App\Model\Product::NUM_ITEMS}}<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu per-page">
                                        <li><a href="#" data-per-page="12">12</a></li>
                                        <li><a href="#" data-per-page="24">24</a></li>
                                        <li><a href="#" data-per-page="48">48</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <span class="catalog-filter__sort-by">Сортировать по :</span>
                                    <a href="#" class="dropdown-toggle sort {{Request::get('sortby') =='price' ? 'active' : ''}}" data-sortdir="{{Request::get('sortdir') =='ASC' ? 'DESC' : 'ASC'}}"  data-sortby="price">
                                        Цене <i class="fa fa-caret-{{Request::get('sortdir') =='ASC' ? 'up' : 'down'}}"></i>
                                    </a>
                                    <a href="#" class="dropdown-toggle sort {{Request::get('sortby') =='hit' ? 'active' : ''}}" data-sortdir="{{Request::get('sortdir') =='ASC' ? 'DESC' : 'ASC'}}"  data-sortby="hit">
                                        Популярности <i class="fa fa-caret-{{Request::get('sortdir') =='ASC' ? 'up' : 'down'}}"></i>
                                    </a>
                                </li>
                                <li class="view-select hidden-xs">
                                    <a href="#" class="view {{Request::get('view') =='block'|| !Request::has('view') ? 'active' : ''}}" data-view="block"><i class="fa fa-th fa-2x" aria-hidden="true"></i></a>
                                    <a href="#" class="view {{Request::get('view') =='list' ? 'active' : ''}}" data-view="list"><i class="fa fa-list fa-2x" aria-hidden="true"></i></a>
                                </li>
                                
                            </ul>
                            <div class="clearfix"></div>
                        </nav>
                        
                        <!--MAIN CATALOG ROW-->
                        
                        <div class="row">
                            @if(count($products)>0)
                                @foreach($products as $k=>$product)
                                    @if(Agent::isMobile())
                                        <div class="col-sm-4">
                                            @include('product.product')
                                        </div>
                                    @else
                                        @if(Request::get('view') =='block'|| !Request::has('view'))
                                            @if($k % 3 == 0)
                                                <div class="clearfix"></div>
                                            @endif
                                            <div class="col-sm-4">
                                                @include('product.product')
                                            </div>
                                        @else
                                            <div class="col-xs-12">
                                                @include('product.product-list')
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            @else
                                <h4 class="text-center">По Вашему запросу ничего не найдено :(</h4>
                                <h4 class="text-center">Попробуйте уменьшить количество параметров поиска</h4>
                            @endif
                        </div>
                        @if(count($products)>0)
                            <nav class="">
                                {!! $products->appends(Request::capture()->except('page'))->render() !!}
                            </nav>
                        @endif
                    </div>
                </div>
            </section>
        </div>
        @if(!empty($lastviews)&& !Agent::isMobile())
            <section class="last-views">
                <div class="header">
                    <div class="row">
                        <div class="col-xs-10">
                            <h4>НЕДАВНО ПРОСМОТРЕННОЕ</h4>
                        </div>
                        <div class="col-xs-2 text-right">
                            <div class="arrows"></div>
                        </div>
                    </div>
                </div>
                
                <div class="views-carousel">
                    @foreach($lastviews as $product)
                        <div class="views-item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="/products/{{$product->slug}}"><img src="{{$product->resized('150x150')[0]}}" alt="" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-8">
                                    <p class="title">
                                        <a href="/products/{{$product->slug}}">{{$product->title}}</a>
                                    </p>
                                    <p class="subtitle">
                                        {{$product->vid}} {{$product->gender}}
                                    </p>
                                    <p class="price">
                                        {{$product->price}} <i class="fa fa-rouble"></i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
        @endif
        <section class="seo">
            {!! $page->seotext !!}
        </section>
@endsection    
@section('js')
@parent
    <script>
        function setCount(){

            $.ajax({
                url: '/catalog',
                data:  $('#filterForm').serialize(),
                processData: true,
                contentType: false,
                type: 'GET',
                success: function(data){
                    //alert(data);
                    $('.count').html(data.total);
                }
            });
        }
        
        $('.per-page a').click(function(){
            $('input[name=per_page]').val($(this).data('per-page'));
            $('#filterForm').submit();
            return false;
        })
        $('.view').click(function(){
            $('input[name=view]').val($(this).data('view'));
            $('#filterForm').submit();
            return false;
        })
        $('.dropdown-toggle.sort').click(function(){
            $(this).toggleClass('active');
            $('input[name=sortdir]').val($(this).data('sortdir'));
            $('input[name=sortby]').val($(this).data('sortby'));
            $('#filterForm').submit();
            return false;
        })


        var slider = $('#price-slider').slider();
        slider.on('change click', function (e) {
            var arr = slider.val().toString().split(',');
            //alert(arr);
            $('#from').val(arr[0]);
            $('#to').val(arr[1]);
            var offset = $('#filterForm').offset();
            setCount();
        });
        var to = $('#to');
        var from = $('#from');
        to.on('change', function () {
            slider.setValue([to.val(),from.val()]);
            setCount();
        });
        from.on('change', function () {
            slider.setValue([to.val(),from.val()]);
            setCount();
        });


        $('#filterForm .styled, .slider').on('click  mousedown',function (e){
            var offset = $('#filterForm').offset();
            setCount();
            var rect = document.getElementById('filterForm').getBoundingClientRect();
            $('.popup-show').css('top',e.pageY -  offset.top - 15);
            /*$('.popup-show').css('position','absolute');
            $('.popup-show').css('transition','1s');
            $('.popup-show').css('left',260 );
            $('.popup-show').css('top',e.pageY -  offset.top - 15);
            $('.popup-show').css('display','block');*/
            $('.popup-show').addClass('active');


        });
        $('.per_page').on('click',function(e){
            $('input[name=per_page]').val($(this).data('val'));
            $('#filterForm').submit();
            return false;
        });
        $('.sortdir').on('click',function(e){
            $('input[name=sortdir]').val($(this).data('val'));
            $('#filterForm').submit();
            return false;
        })
        $('.views-carousel').slick({
            infinite: true,
            dots:false,
            slidesToShow: 3,
            autoplay: false,
            slidesToScroll: 1,
            appendArrows: '.arrows',
            prevArrow:'<button type="button" class="slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
            nextArrow:'<button type="button" class="slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>',
        });

    </script>
@endsection  
