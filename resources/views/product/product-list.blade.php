                <div class="item {{$product->gender}} list" data-pid="{{$product->id}}">
                    @if($product->new)
                        <div class="new">NEW</div>
                    @endif
                    @if($product->price_sale>0)
                        <div class="sale"></div>
                    @endif
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="image">
                                <a href="/products/{{$product->slug}}" data-pid="{{$product->id}}">
                                    <img src="/{{$product->resized('300x300')[0]}}" alt="{{$product->title}}" class="img-responsive">
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="content">
                                <a class="wishlist js-wishlist  @if(session()->has('wishlist')) @if(in_array($product->id, session()->get('wishlist'))) active @endif @endif" href="#" data-pid="{{$product->id}}"></a>

                                <p class="title"><a href="/products/{{$product->slug}}">{{$product->title}}</a></p>
                                <p class="description">{{$product->vid}} {{$product->gender}}</p>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="shop-block">
                                <div class="price-block reg">
                                    <span class="price-type">
                                        {{--опт.:</span><span class="price">{{$product->price}} <i class="fa fa-rub" aria-hidden="true"></i>--}}
                                        Цена скрыта
                                    </span>
                                </div>
                                <div class="buy-block">
                                    <form action="{{route('cart.store')}}" method="POST" class="buy-form js-cart-add-form" data-pid="{{$product->id}}">
                                        {!! csrf_field() !!}
                                        <input type="hidden" value="{{$product->id}}" name="pid" class="pid">
                                        <input type="hidden" value="1" name="count">
                                        <button type="submit" class="btn btn-buy js-cart-add">В КОРЗИНУ</button>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                </div>
