@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists('product', $product) !!}
        @include('includes/details')
        <section class="popular carousel">
            <h2 class="section-title">Рекомендуем</h2>
            
            <div class="items-carousel js-items-carousel">
                @foreach($recommends as $product)
                    @include('product.product')
                @endforeach
            </div>

        </section>        
        @if(!empty($lastviews) && !Agent::isMobile())

            <section class="last-views">
                <div class="header">
                    <div class="row">
                        <div class="col-xs-10">
                            <h4>НЕДАВНО ПРОСМОТРЕННОЕ</h4>
                        </div>
                        <div class="col-xs-2 text-right">
                            <div class="arrows"></div>
                        </div>
                    </div>
                </div>
                
                <div class="views-carousel">
                    @foreach($lastviews as $product)
                        <div class="views-item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="/products/{{$product->slug}}"><img src="/{{$product->resized('150x150')[0]}}" alt="" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-8">
                                    <p class="title">
                                        <a href="/products/{{$product->slug}}">{{$product->title}}</a>
                                    </p>
                                    <p class="subtitle">
                                        {{$product->vid}} {{$product->gender}}
                                    </p>
                                    <p class="price">
                                        {{$product->price}} <i class="fa fa-rouble"></i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
        @endif
        
        <!--section class="seo">
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur</p>
        </section-->
        
@endsection    
@section('js')
@parent
    <script>
        $('.main-image').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            slide: '.slide',
            asNavFor: '.thumbs',
            autoplay: true,
            //adaptiveHeight: true,
            autoplaySpeed: 3000,
        });
        $('.thumbs').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.main-image',
            dots: false,
            vertical: false,
            verticalSwiping:true,
            infinite: true,
            adaptiveHeight: true,
            focusOnSelect: true,
            arrows: false,
        });   

        $('.views-carousel').slick({
            infinite: true,
            dots: false,
            slidesToShow: 3,
            autoplay: false,
            slidesToScroll: 1,
            appendArrows: '.arrows',
            prevArrow:'<button type="button" class="slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
            nextArrow:'<button type="button" class="slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>',
        });
    </script>
@endsection  
