                <div class="item {{$product->gender}}" data-pid="{{$product->id}}">
                    @if($product->new==1)
                        <div class="new">NEW</div>
                    @endif
                    @if($product->price_sale>0)
                        <div class="sale"></div>
                    @endif
                    <a class="wishlist js-wishlist  @if(session()->has('wishlist')) @if(in_array($product->id, session()->get('wishlist'))) active @endif @endif" href="#" data-pid="{{$product->id}}"></a>
                    <div class="image">
                        <a href="/products/{{$product->slug}}">
                            @if(Agent::isDesktop())
                                <div class="js-fastview fastview" data-pid="{{$product->id}}"></div>
                            @endif
                            <img src="/{{$product->resized('300x300')[0]}}" alt="{{$product->title}}" class="img-responsive">
                        </a>
                    </div>
                    <div class="content">
                        <p class="title"><a href="/products/{{$product->slug}}">{{$product->title}}</a></p>
                        <p class="description">{{$product->vid}}</p>
                    </div>
                    <div class="shop-block">

                        <div class="price-block reg">
                            <span class="price-type">
                                {{--опт.:</span><span class="price">{{$product->price}} <i class="fa fa-rub" aria-hidden="true"></i>--}}
                                Цена скрыта
                            </span>
                        </div>
                        {{--
                            <div class="price-block">
                                <div class="">
                                    <span class="price">{{$product->price_opt}} <i class="fa fa-rub" aria-hidden="true"></i></span>
                                    @if($product->old_price>0)
                                        <span class="old-price">{{$product->old_price}} <i class="fa fa-rub" aria-hidden="true"></i></span>
                                    @endif
                                </div>
                            </div>

                            @else
                                @if(Auth::user()->opt==1)
                                    <div class="price-block reg">
                                        <span class="price-type">опт.:</span><span class="price">{{$product->price}} <i class="fa fa-rub" aria-hidden="true"></i></span>
                                    </div>
                                @else
                                    <div class="price-block reg">
                                        <!--span class="price-type">розн.:</span-->
                                        <span class="price">{{$product->price}} <i class="fa fa-rub" aria-hidden="true"></i></span>
                                        @if($product->old_price>0)
                                            <span class="old-price">{{$product->old_price}} <i class="fa fa-rub" aria-hidden="true"></i></span>
                                        @endif
                                    </div>
                                @endif
                            @endif
                            --}}
                        <div class="buy-block">
                            <form action="{{route('cart.store')}}" method="POST" class="buy-form js-cart-add-form" data-pid="{{$product->id}}">
                                {!! csrf_field() !!}
                                <input type="hidden" value="{{$product->id}}" name="pid" class="pid">
                                <input type="hidden" value="1" name="count">
                                <button type="submit" class="btn btn-buy js-cart-add">В КОРЗИНУ</button>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
