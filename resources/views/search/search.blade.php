@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists() !!}
    <!--SEARCH PAGE-->
    <section class="search">
            
            <h1>Результаты поиска</h1>
            <h3>По запросу "<strong>{{$q}}</strong>" найдено: {{$products->total()}} товаров</h3>
            <div class="row content">

                <div class="col-sm-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                        <h3>При заполнении формы возникли ошибки!</h3>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                @if(count($products)>0)
                    @foreach($products as $k=>$product)
                    	@if($k%3==0)
                    		<div class="clearfix"></div>
                    	@endif
                        <div class="col-xs-4">
                            @include('product.product')
                        </div>

                     @endforeach   
                     <div class="clearfix"></div>
                     <div class="text-center col-xs-12">
                         {!! $products->appends(Request::capture()->except('page'))->render() !!}
                     </div>
                    
                @else
                    <div class="text-center">
                       <h3>Ничего не найдено</h3>
                    </div>
                @endif
            </div>

    </section>


@endsection

@section('js')
@parent
<script src="/js/product-common.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.5.3/bootstrap-slider.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.5.3/css/bootstrap-slider.min.css" />

    
@endsection  

