    @if(isset($products))
        @foreach($products as $product)
            <li>
                <a href="products/{{$product->slug}}">
                    <div class="img-holder">
                        @if(isset($product->images[0]))
                            <img src="{{$product->image}}" class="img-responsive"></div>
                        @else
                            <img src="https://dummyimage.com/50x50/cccccc/0011ff" class="img-responsive"></div>
                        @endif
                    <div class="content-holder">
                        <h6 class=""> {{$product->title}}</h6>
                        <p class="price"> {{$product->price}}<i class="fa fa-rouble"></i></p>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
        @endforeach
    @endif
