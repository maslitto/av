@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists() !!}
    <!--CONTENT PAGE-->
    <section id="content">

            <h1 class="section-title">{{$page->title}}</h1>
                <div class="row">

                <div class="col-sm-12 content">
                <div style="color:#333;padding:0 15px;font-weight:normal !important;font-family: 'Roboto v15' !important;">
                    {!! $page->content !!}
                </div>    
                </div>
            </div>

    </section>

@endsection



