@extends('emails.base')

@section('title')
	Подтверждение регистрации на сайте angelo-vani.ru !
@endsection

@section('content')
	<p>Для подтверждения регистрации, пройдите по  <a href="http://angelo-vani.ru/register/confirm/{{$token}}"> ссылке</a></p>
	<p>Если Вы не совершайте никаких действий на сайте angelo-vani.ru, просто проинорируйте это письмо.</p>
@endsection
