@extends('emails.base')

@section('title')
	Регистрация на сайте angelo-vani.ru
@endsection

@section('content')
	<p>Вы успешно зарегистрированы на сайте <a href="{{env('APP_URL')}}">{{env('APP_URL')}}</a>
		Ваш логин на сайте <strong>{{$user->email}}</strong>. Пароль <strong>{{$password}}</strong></p>
	<p>Изменить пароль и контактные данные Вы можете на в личном кабинете  <a href="{{env('APP_URL')}}/profile">по ссылке</a></p>
@endsection


