@extends('emails.base')

@section('title')
	Подтверждение подписки на сайте angelo-vani.ru
@endsection

@section('content')
	<p>Для подтверждения подписки, пройдите по  <a href="{{env('APP_URL')}}/subscribe/confirm/{{$token}}"> ссылке</a></p>
	<p>Если Вы не совершайте никаких действий на сайте angelo-vani.ru, просто проинорируйте это письмо.</p>
@endsection
