<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tbody>
        <tr>
            <td align="center" valign="top">
            
            <table style="border:1px solid #dddddd;background-color:#ffffff;border-radius:4px" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody>
                    <tr>
                        <td align="center" valign="top">
                        <table style="background-color:#ffffff;border-bottom:0" border="0" cellpadding="0" cellspacing="0" width="600">
                            <tbody>
                                <tr style="background:#222">
                                    <!--td style="width: 216px; padding-left: 15px;" valign="middle"><img src="http://anggelo-vani.ru/template/img/logo.png" width=200></td-->
                                    <td style="padding-top: 10px;" valign="middle">
                                        <h2 style="text-align: center; color: #fff; font-size: 40px; margin: 15px 0px;">ANGELO-VANI.RU</h2>
                                        <p style="text-align: center; color: #ddd; font-size: 24px;">Интернет-магазин сумок и аксессуаров</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="600">
                            <tbody>
                                <tr>
                                    <td valign="top">
                                    <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td valign="top">
                                                <div style="color:#333333;font-family:Arial;font-size:14px;line-height:150%;text-align:left;background-color:#ffffff">
                                                    <h2 style="color:#202020;display:block;font-family:Arial;font-size:24px;font-weight:bold;line-height:100%;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left">
                                                        @yield('title')
                                                    </h2>
                                                    <hr style="margin:18px 0;border:0;border-top:1px solid #eeeeee;border-bottom:1px solid #ffffff">
                                                    @yield('content')
                                                </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>