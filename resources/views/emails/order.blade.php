@extends('emails.base')

@section('title')
	Новый заказ на сайте angelo-vani.ru
@endsection

@section('content')
	<p>Имя: {{$order->name}}</p>
	<p>Телефон: {{$order->phone}}</p>
	<p>E-mail: {{$order->email}}</p>
	<p>Адрес: {{$order->index}}, {{$order->country}}, г. {{$order->city}}, ул. {{$order->street}}, д. {{$order->home}}, кв. {{$order->apt}}</p>
	<p>Комментарий: {{$order->comment}}</p>
	<p>Способ оплаты: {{$order->payment}}</p>
	<p>Способ доставкти: {{$order->delivery}}</p>
	<table>
		<tr>
			<th style="text-align:center;width: 50px;vertical-align:middle">Артикул</th>
			<th style="text-align:center;width: 150px">Цена</th>
			<th style="text-align:center;width: 50px">Количество</th>
			<th style="text-align:center;width: 150px">Сумма</th>
		</tr>
		@foreach($order->products as $item)
			<tr>
				<!--td>{{$item->product->article}}</td-->
				<td style="text-align:center;width: 50px">{{$item->product->title}}</td>
				<td style="text-align:center;width: 150px">{{number_format($item->product->price, 0, ',', ' ') }} руб.</td>
				<td style="text-align:center;width: 50px">{{$item->qty}}</td>
				<td style="text-align:center;width: 150px">{{number_format($item->getTotalPrice(), 0, ',', ' ') }} руб.</td>
			</tr>
		@endforeach
		<tr>
			<td colspan=4 style="text-align:right">ИТОГО: <strong>{{$order->order_cost}} руб.</strong></td>
		</tr>
	</table>

@endsection


