@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::render('post', $post) !!}
    <!--CART-->
    <p class="section-title">Блог</p>
    <section class="blogs">
        <div class="content">
            <h1 class="section-title">{{$page->title}}</h1>
            <div class="text-center">
                <img src="{{$page->image}}" height=300>
            </div>
            <hr>
            <div class="text">
                {!! $page->text !!}
            </div>
        </div>

    </section> 
@endsection    
@section('js')
    @parent
@endsection
