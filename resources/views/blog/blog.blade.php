@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists() !!}
    <!--CART-->
    <section class="blogs">
        <h1 class="section-title">Блог</h1>
        <div class="content">
            <div class="row">
            @foreach($blogs as $k=>$post)
                @if($k % 3 == 0)
                    <div class="clearfix"></div>
                @endif
                <div class="col-xs-4">
                    <div class="image">
                        <img src="{{$post->image}}" alt="{{$post->title}}" class="img-responsive">
                    </div>
                    <div class="text">
                        <p class="small date">{{$post->created_at}}</p>
                        <p class="lead" ><a href="{{$post->url}}">{{$post->title}}</a></p>
                        <div class="small">
                            {!! \Illuminate\Support\Str::words($post->text, 20,'....')  !!}
                            <p><a href="{{$post->url}}" class="pink">Читать дальше</a></p>
                        </div>
                    </div>
                </div>

            @endforeach
            </div>
            <div class="text-center">{{$blogs->links()}}</div>
        </div>

    </section> 


@endsection    
@section('js')
    @parent
@endsection
