@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists() !!}
        <section class="checkout">
            <h1 class="section-title">Оформление заказа</h1>
            @if(session()->get('step')==5)
                <div class="jumbotron">
                    <h1 class="text-center">Ваш заказ успешно оформлен!</h1>
                    <h3 class="text-center">Вы успешно оформили заказ № {{$order->id}}</h3>
                    <h3 class="text-center">В ближайшее время наши менеджеры обработают заказ и свяжутся с Вами для подтверждения заказа</h3>
                    <br><br>
                    <p class="text-center"><a class="btn btn-primary btn-lg" href="/">Вернуться на главную</a></p>
                </div>
            @elseif(Cart::count()<1)
                <div class="jumbotron">
                    <h1 class="text-center">Ваша корзина пуста</h1>
                    <h3 class="text-center">Чтобы оформить заказ наберите товар в корзину</h3>
                    <br><br>
                    <p class="text-center"><a class="btn btn-primary btn-lg" href="/catalog">Перейти в каталог</a></p>
                </div>
            @else
            <div class="row">
                <div class="col-sm-9">
                    <div class="step-content">
                        <div class="steps">
                            <ul class="valign">
                                <li class="finished"><span>1</span> <strong>Корзина</strong></li>
                                <li class="@if(session()->get('step')==2) active @else finished @endif"><span>2</span> <strong>Адрес</strong></li>
                                <li class="@if(session()->get('step')==3) active @elseif(session()->get('step')>3)  finished @endif"><span>3</span> <strong>Доставка</strong></li>
                                <li class="@if(session()->get('step')>=4) active  @endif"><span>4</span> <strong>Оплата</strong></li>
                            </ul>
                        </div>
                        @if(session()->get('step')==2)
                            <form action="/order/step3" method="post">
                                {!! csrf_field() !!}
                                <h4>ПЕРСОНАЛЬНЫЕ ДАННЫЕ</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>ИМЯ <span class="required">*</span></label>
                                            <input type="text" name="name" class="form-control" required value="{{!Auth::guest() ? Auth::user()->name : ''}}">
                                        </div>
                                        <div class="form-group">
                                            <label>E-mail <span class="required">*</span></label>
                                            <input type="email" name="email" class="form-control" required value="{{!Auth::guest() ? Auth::user()->email : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Фамилия</label>
                                            <input type="text" name="second_name" class="form-control" required value="{{!Auth::guest() ? Auth::user()->second_name : ''}}">
                                        </div>
                                        <div class="form-group">
                                            <label>ТЕлефон <span class="required">*</span></label>
                                            <input type="text" name="phone" class="form-control" required value="{{!Auth::guest() ? Auth::user()->phone : ''}}">
                                        </div>
                                    </div>
                                </div>
                                <h4>адрес доставки</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Страна</label>
                                            <input type="text" name="country" class="form-control" value="{{!Auth::guest() ? Auth::user()->country : ''}}">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Индекс</label>
                                                    <input type="text" name="index" class="form-control" value="{{!Auth::guest() ? Auth::user()->index : ''}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Улица</label>
                                                    <input type="text" name="street" class="form-control" value="{{!Auth::guest() ? Auth::user()->street : ''}}">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Город</label>
                                            <input type="text" name="city" class="form-control" value="{{!Auth::guest() ? Auth::user()->city : ''}}">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Дом</label>
                                                    <input type="text" name="home" class="form-control" value="{{!Auth::guest() ? Auth::user()->home : ''}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Квартира</label>
                                                    <input type="text" name="apt" class="form-control" value="{{!Auth::guest() ? Auth::user()->apt : ''}}">
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <hr>
                                <div class="buttons-block valign">
                                    <a href="/cart" class="btn btn-prev"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Вернуться</a>
                                    <button class="btn btn-next btn-default" type="submit"> ПРОДОЛЖИТЬ <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                </div>
                            </form>
                        @elseif(session()->get('step')==3)
                            <form action="/order/step4" method="post">
                                {{Form::token()}}
                                {{--dd(session()->all())--}}
                                <h4>Выберите способ доставки</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="radio radio-primary">
                                            <input id="delivery1" class="styled" name="delivery" value="Курьер" required type="radio">
                                            <label for="delivery1">
                                                <strong>КУРЬЕРОМ</strong> - 200 <i class="fa fa-rouble"></i>
                                                <p>Курьер по Новостибирску. Срок доставки 1-2 рабочих дня, в зависимости от времени оформления заказа.</p>
                                            </label>

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio radio-primary">
                                            <input id="delivery2" class="styled" name="delivery" value="Самовывоз" required type="radio" checked>
                                            <label for="delivery2">
                                                <strong>САМОВЫВОЗ</strong> - БЕСПЛАТНО
                                                <p>Самостоятельное полуение товара с нашего склада в Новосибирске</p>
                                            </label>

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio radio-primary">
                                            <input id="delivery3" class="styled" name="delivery" value="Почта России" required type="radio">
                                            <label for="delivery3">
                                                <strong>ПОЧТА РОССИИ</strong> - 300 <i class="fa fa-rouble"></i>
                                                <p>Срок поставки зависит от работы Почты России</p>
                                            </label>

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio radio-primary">
                                            <input id="delivery4" class="styled"  name="delivery" value="Траспортная компания" required type="radio">
                                            <label for="delivery4">
                                                <strong>ДОСТАВКА ДО ТРАНСПОРТНОЙ КОМПАНИИ</strong> - БЕСПЛАТНО
                                                <p>Деловые линии / Энергия  / Грузовая почта ПЭК  /  Ратек</p>
                                            </label>

                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <hr>
                                        <label for="comment">Комментарий к заказу</label>
                                        <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                </div>

                                <hr>
                                <div class="buttons-block valign">
                                    <a href="{{url()->previous()}}" class="btn btn-prev"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Вернуться</a>
                                    <button class="btn btn-next btn-default" type="submit"> ПРОДОЛЖИТЬ <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                </div>
                            </form>
                        @elseif(session()->get('step')==4)
                            <form action="/order/finish" method="post">
                                {{Form::token()}}
                                <h4>Выберите способ оплаты</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="radio radio-primary">
                                            <input id="payment1" class="styled" name="payment" value="Наличными при получении" required type="radio" checked>
                                            <label for="payment1">
                                                <strong>НАЛИЧНЫМИ</strong>
                                                <p>Оплата наличными при получении товара</p>
                                            </label>

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio radio-primary">
                                            <input id="payment2" class="styled" name="payment" value="ОН-ЛАЙН ОПЛАТА" required type="radio" >
                                            <label for="payment2">
                                                <strong>ОН-ЛАЙН ОПЛАТА</strong>
                                                <p>Банковские карты, терминалы и т.д.</p>
                                            </label>

                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="buttons-block valign">
                                    <a href="{{url()->previous()}}" class="btn btn-prev"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Вернуться</a>
                                    <button class="btn btn-next btn-default" type="submit"> ПРОДОЛЖИТЬ <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="cart-content">
                        <h4 class="text-center">ВАШ ЗАКАЗ</h4>
                        <ul class="products">
                            @foreach(Cart::content() as $item)
                                <li class="basket-item">
                                    <div class="image">
                                        <a href="{{$item->model->url}}"><img src="{{$item->model->resized('150x150')[0]}}" alt="" class="img-responsive"></a>
                                    </div>
                                    <div class="text">
                                        <p class="title"><a href="{{$item->model->url}}">{{$item->model->title}}</a></p>
                                        <p class="subtitle">{{$item->model->vid}} {{$item->model->gender}}</p>
                                        <p class="count">{{$item->qty}} x <span class="sum">{{ number_format($item->model->price, 0, ',', ' ') }} <i class="fa fa-rouble"></i></span></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            @endforeach

                        </ul>
                        <ul class="sums">
                            <li class="valign">
                                <span>Доставка</span> <span class="price">0 <i class="fa fa-rouble"></i></span>
                            </li>
                            <!--li class="valign"><span>Скидка</span> <span class="price">800 <i class="fa fa-rouble"></i></span></li-->
                            <li class="valign"><span>Итого</span> <span class="total">{{Cart::subtotal(0,'',' ')}} <i class="fa fa-rouble"></i></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            @endif
        </section>
@endsection
@section('js')
    @parent
    <link rel="stylesheet" href="template/css/bootstrap-slider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" />
@endsection