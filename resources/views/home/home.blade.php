@extends('layouts.app')

@section('content')
        <section class="banner js-banner hidden-xs">
            @if(!empty($banners))
                @foreach($banners as $banner)
                    <div class="slide">
                        <img src="{{$banner->resized('1170x390')}}" alt="{{$banner->title}}">
                        <div class="text">
                            <p>Новая коллекция</p>
                            <p class="banner-header">
                                {{$banner->title}}
                            </p>
                            <p class="description">
                                {{$banner->text}}
                            </p>
                            <p><a href="{{$banner->url}}" class="btn btn-default">ПЕРЕЙТИ</a></p>
                        </div>
                    </div>
                @endforeach
            @endif
           
        </section>
        
        <section class="pluses hidden-xs">
            <div class="row">
                <div class="col-xs-3 text-center">
                    <p><img src="/template/img/plus-1.png" alt="Бесплатная доставка"></p>
                    <p class="title">БЕСПЛАТНАЯ ДОСТАВКА</p>
                    <p class="description">Бесплатная доставка при  заказе от 10000 рублей</p>
                </div>
                <div class="col-xs-3 text-center">
                    <p><img src="/template/img/plus-2.png" alt="На рынке 10 лет"></p>
                    <p class="title">На рынке более 10 лет</p>
                    <p class="description">Больше 10 лет предолеваем все кризисы и невзгоды</p>
                </div>
                <div class="col-xs-3 text-center">
                    <p><img src="/template/img/plus-3.png" alt="ДОСТАВКА ПО ВСЕЙ РОССИИ"></p>
                    <p class="title">ДОСТАВКА ПО ВСЕЙ РОССИИ</p>
                    <p class="description">Доставка по всей России при оплате любым удобным способом</p>
                </div>
                <div class="col-xs-3 text-center">
                    <p><img src="/template/img/plus-4.png" alt="Гарантия качества"></p>
                    <p class="title">Гарантия качества</p>
                    <p class="description">Даем гарантию на продукцию 30 дней</p>
                </div>
            </div>
        </section>
        @if(Agent::isMobile() || Agent::isTablet())
            <section class="categories">
                <a href="/catalog/zhenskie-sumki" class="categories__link">
                    <img src="/template/img/women-mob.jpg" alt="Женские сумки: Мягкие, хобо, тоут, клатч, фреймбег">
                    <div class="text" >
                        <p class="title">Сумки для <br> Женщин</p>
                        <p class="subtitle">Мягкие, хобо, тоут, клатч, фреймбег</p>
                        <p class="total"> {{App\Model\Product::active()->women()->count()}} моделей</p>
                    </div>
                </a>
                <a href="catalog/muzhskie-sumki" class="man categories__link">
                    <img src="/template/img/men-mob.jpg" alt="Мужские сумки: Портфели, мессенджеры, портмоне, барсетки">
                    <div class="text">
                        <p class="title">Сумки для <br> Мужчин</p>
                        <p class="subtitle">Портфели, мессенджеры, портмоне, барсетки</p>
                        <p class="total">{{App\Model\Product::active()->men()->count()}} моделей</p>
                    </div>
                </a>
                <a href="/catalog/" class="sale categories__link">
                    <img src="/template/img/sale-mob.jpg" alt="Распродажа сумок">
                    <div class="text">
                        <p class="title">Cкидки до 50% <br> на сумки </p>
                        <p class="subtitle">Распродажа сумок из прошлых коллекций</p>
                        <p class="total">{{App\Model\Product::where('price_sale','>',0)->count()}} моделей</p>
                    </div>
                </a>
                {{--<a href="/catalog/aksessuary" class="accesories categories__link">
                    <img src="/template/img/acc-mob.jpg" alt="Женские сумки">
                    <div class="text">
                        <p class="title">Аксессуары <br> к сумкам</p>
                        <p class="subtitle">Различные сопутствующие товары</p>
                        <p class="total">Более {{App\Model\Product::whereIn('category_id',App\Model\Page::where('title','Аксессуары')->first()->getDescendants(1, array('id')))->count()}} моделей</p>
                    </div>
                </a>
                --}}
            </section>
        @else
            {{--<section class="categories">
                <div class="picture-block">
                    <a href="/catalog/zhenskie-sumki" class="woman categories__link">
                        <img src="/template/img/women.jpg" alt="Женские сумки: Мягкие, хобо, тоут, клатч, фреймбег">
                        <div class="text">
                            <p class="title">Сумки для <br> Женщин</p>
                            <p class="subtitle">Мягкие, хобо, тоут, клатч, фреймбег</p>
                            <p class="total"> {{App\Model\Product::active()->women()->count()}} моделей</p>
                        </div>
                    </a>

                    <a href="catalog/muzhskie-sumki" class="man categories__link">
                        <img src="/template/img/men.jpg" alt="Мужские сумки: Портфели, мессенджеры, портмоне, барсетки">
                        <div class="text">
                            <p class="title">Сумки для <br> Мужчин</p>
                            <p class="subtitle">Портфели, мессенджеры, портмоне, барсетки</p>
                            <p class="total">{{App\Model\Product::active()->men()->count()}} моделей</p>
                        </div>
                    </a>

                    <a href="/catalog/" class="sale categories__link">
                        <img src="/template/img/sale.jpg" alt="Распродажа сумок">
                        <div class="text">
                            <p class="title">Cкидки до 50% на сумки<br> из прошлых коллекций</p>
                            <p class="subtitle">Интересные предложения по приятным ценам</p>
                            <p class="total">{{App\Model\Product::where('price_sale','>',0)->count()}} моделей</p>
                        </div>
                    </a>
                </div>

                <div class="picture-block">
                    <a href="/catalog/aksessuary" class="accesories categories__link">
                        <img src="/template/img/accessories.jpg" alt="Женские сумки">
                        <div class="text">
                            <p class="title">Аксессуары <br> к сумкам</p>
                            <p class="subtitle">Различные сопутствующие товары</p>
                            <p class="total">Более {{App\Model\Product::whereIn('category_id',App\Model\Page::where('title','Аксессуары')->first()->getDescendants(1, array('id')))->count()}} моделей</p>
                        </div>
                    </a>
                </div>
            </section>
            --}}
        @endif


        <section class="popular carousel">
            <h2 class="section-title">Новинки</h2>
            <nav class="text-center selector">
                <ul>
                    <li><a href="#" class="active js-selector" data-filter="all">Все</a></li>
                    <li><a href="#" class="js-selector" data-filter="Женская">Для женщин</a></li>
                    <li><a href="#" class="js-selector" data-filter="Мужская">Для мужчин</a></li>
                    <li><a href="#" class="js-selector" data-filter="accessories">Аксессуары</a></li>
                </ul>
            </nav>
            <div class="items-carousel js-items-carousel">
                @foreach($populars as $product)
                    @include('product.product')
                @endforeach
            </div>
            <div class="text-center">
                <a href="/catalog" class="btn btn-default btn-lg"><i class="fa fa-repeat" aria-hidden="true"></i> хочу больше</a>
            </div>
        </section>
        @if(Agent::isDesktop())
            {{--<section class="last-news">
                <div class="header">
                    <div class="row">
                        <div class="col-xs-10">
                            <h3>ПОСЛЕДНИЕ НОВОСТИ</h3>
                        </div>
                        <div class="col-xs-2 text-right">
                            <div class="arrows js-arrows"></div>
                        </div>
                    </div>
                </div>
                <div class="news-carousel js-news-carousel">
                    @foreach($news as $item)
                        <div class="news-item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="{{$item->url}}">
                                        <img src="{{$item->image}}" alt="{{$item->title}}" class="img-responsive">
                                    </a>
                                </div>
                                <div class="col-sm-8">
                                    <div class="datetime">
                                        {{$item->created_at->format('d-m-Y')}}
                                    </div>
                                    <p class=""><a href="{{$item->url}}">{{$item->title}}</a></p>
                                    <div class="introtext">
                                        {!! \Illuminate\Support\Str::words($item->text, 20,'....')  !!}
                                        <p><a href="{{$item->url}}" class="pink">Читать дальше</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </section>

            <section class="social-subscribe">
            <div class="row">
                <div class="col-xs-7 ">
                    <div class="subscribe">
                        <div class="row">
                            <div class="col-xs-5">
                                Подписывайтесь на наши акции и лучшие предложения
                            </div>
                            <div class="col-xs-7">
                                <form action="#" method="POST" class="subscription-form">
                                    {!! csrf_field() !!}
                                    {{Form::hidden('robot')}}
                                    <input type="email" name="email" class="form-control" placeholder="Укажите Ваш e-mail">
                                    <button type="submit"></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5">
                    <div class="social">
                        <div class="row">
                            <div class="col-xs-5">
                                Присоединяйтесь к нам в соцсетях
                            </div>
                            <div class="col-xs-7 text-center">
                                <a href="https://vk.com/angelovani" target="_blank" rel="nofollow"><i class="fa fa-vk fa-2x" aria-hidden="true"></i></a>
                                <a href="https://www.instagram.com/angelo.vani/" target="_blank" rel="nofollow"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
                                <!--a href="#" target="_blank"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></a>
                                <a href="#" target="_blank"><i class="fa fa-odnoklassniki fa-2x" aria-hidden="true"></i></a-->
                            </div>
                        </div>
                        
                    </div>    
                </div>
                
            </div>
        </section>
        --}}
        @endif
        @if(count($sales))
            <section class="sale carousel">
                <h2 class="section-title">Распродажа</h2>
                <nav class="text-center selector">
                    <ul>
                        <li><a href="#" class="active js-selector" data-filter="all">Все</a></li>
                        <li><a href="#" class="js-selector" data-filter="Женская">Для женщин</a></li>
                        <li><a href="#" class="js-selector" data-filter="Мужская">Для мужчин</a></li>
                        <li><a href="#" class="js-selector" data-filter="accessories">Аксессуары</a></li>
                    </ul>
                </nav>
                <div class="items-carousel js-items-carousel">
                    @foreach($sales as $product)
                        @include('product.product')
                    @endforeach
                </div>
                <div class="text-center">
                    <a href="/catalog" class="btn btn-default btn-lg"><i class="fa fa-repeat" aria-hidden="true"></i> хочу больше</a>
                </div>
            </section>
        @endif
        <section class="about">
            <h2 class="section-title">О компании</h2>
            <div class="about__text text">
                <p class="text-center">Интернет-магазин Angelo Vani предлагает обширный каталог женских и мужских сумок из натуральной и искусственной кожи оптом и в розницу. Все модели фабричного производства Китай. Хорошего качества, в богатой цветовой гамме, по демократичным ценам.</p>
                <p class="text-center">Представленные модели разнообразны по дизайну, размеру, форме, материалу. Но все их отличает удобство в использовании и функциональность.</p>
                <p class="text-center">Мы предлагаем актуальные кожаные сумки, в том числе вместительные летние сумки-трансформеры из текстиля, компактные дневные и вечерние клатчи, плоские портмоне-органайзеры для документов.</p> 
                <p class="text-center"><a href="/o-kompanii" class="more">Узнать больше</a></p>
            </div>
            <div class="pictures valign">
                <div class="picture">
                    <img class="img-responsive"  src="/template/img/about1.jpg" alt="">
                </div>
                <div class="picture">
                    <img class="img-responsive" src="/template/img/about21.jpg" alt="">
                    <img class="img-responsive" src="/template/img/about22.jpg" alt="">
                </div>
                <div class="picture">
                    <img class="img-responsive" src="/template/img/about3.jpg" alt="">
                </div>
                <div class="picture">
                    <img class="img-responsive" src="/template/img/about4.jpg" alt="">
                </div>
                <div class="clearfix"></div>
            </div>

        </section>
        
        <section class="seo">
            {!! $page->seotext !!}
        </section>
@endsection