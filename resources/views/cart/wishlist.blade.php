@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists() !!}
    <section class="profile">
        <h2 class="section-title">Избранное</h2>

        <div class="content">
            @if(!empty($wishlist))
                <div class="wishlist cart">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="2">Товар</th>
                                <th>Цвет</th>
                                <th>Артикул</th>
                                <th>Цена</th>
                                <th></th>
                                <th><i class="fa fa-trash"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($wishlist as $item)
                                <tr data-pid="{{$item->id}}">
                                    <td>
                                        <img src="{{$item->resized('150x150')[0]}}" alt="" class="img-responsive">
                                    </td>
                                    <td>
                                        <p class="title"><a href="{{$item->url}}"></a></p>
                                        <p class="subtitle">{{$item->vid}} {{$item->gender}}</p>
                                    </td>
                                    <td>{{$item->color}}</td>
                                    <td>{{$item->title}}</td>

                                    <td><span class="price">{{$item->price}} <i class="fa fa-rouble"></i></span></td>
                                    <td>
                                        <a href="#" class="btn btn-default to-cart">В КОРЗИНУ</a>
                                    </td>
                                    <td><a href="#" class="delete"><img src="/template/img/icon-close.png" alt=""></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>

                            <tr class="buttons">
                                <td colspan="4"> <a href="#" class="btn btn-grey destroy"><img src="/template/img/icon-close.png" alt="" width=13>  Удалить все</a></td>
                                <td colspan="3" class="text-right"> <a href="#" class="btn btn-primary buy-all">Добавить все в корзину</a></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            @else
                <div class="jumbotron">
                    <h1 class="text-center">В избранном пусто :(</h1>
                    <br><br>
                    <p class="text-center"><a class="btn btn-primary btn-lg" href="/catalog">Перейти в каталог</a></p>
                </div>
            @endif


        </div>

    </section>
@endsection    
@section('js')
    @parent
    <script>
        function showEmpty(){
            $('.wishlist.cart').html('<div class="jumbotron">\
                <h1 class="text-center">В избранном пусто :(</h1>\
            <br><br>\
            <p class="text-center"><a class="btn btn-primary btn-lg" href="/catalog">Перейти в каталог</a></p>\
            </div>');
        }
        $('.to-cart').click(function() {
            btn = $(this);
            product = $(this).closest('tr');
            console.log(product.data('pid'));

            $.ajax({
                url: '{{route("cart.store")}}',
                type: "post",
                data: {
                    pid: product.data('pid'),
                    _token: token,
                },
                success: function (response) {
                    //alert(response.message);
                    new Noty({
                        type: 'success',
                        layout: 'center',
                        theme : 'relax',
                        text: response.message
                    }).show();
                    $('.basket .total').html(response.count);
                    $('.wishlist-header .total').html(response.count);
                    $('#basket-cont').html(response.content);
                    $.ajax({
                        url: '{{ route('wishlist.store') }}',
                        type: "post",
                        data: {
                            pid: product.data('pid'),
                            _token: token,
                        },
                        success: function (response) {
                            if(response.count ==0) showEmpty();
                        }
                    });
                    product.remove();
                },
                error: function (jqXHR, exception) {

                },
            });

            return false;
        });
        $('.delete').click(function() {
            btn = $(this);
            product = $(this).closest('tr');
            console.log(product.data('pid'));
            $.ajax({
                url: '{{ route('wishlist.store') }}',
                type: "post",
                data: {
                    pid: product.data('pid'),
                    _token: token,
                },
                success: function (response) {
                    product.remove();
                    $('.wishlist-header .total').html(response.count);
                    if(response.count ==0) showEmpty();
                },
                error: function (jqXHR, exception) {

                },
            });
            return false;
        });

        $('.destroy').click(function() {
            btn = $(this);
            $.ajax({
                url: '/wishlist/destroy',
                type: "post",
                data: {
                    _token: token,
                },
                success: function (response) {
                    $('.wishlist-header .total').html(response.wcount);
                    shoEmpty();
                },
                error: function (jqXHR, exception) {

                },
            });
            return false;
        });
        $('.buy-all').click(function() {
            btn = $(this);
            $.ajax({
                url: '/wishlist/buyall',
                type: "post",
                data: {
                    _token: token,
                },
                success: function (response) {
                    showEmpty();
                    $('.basket .total').html(response.count);
                    $('.wishlist-header .total').html(response.wcount);
                    $('#basket-cont').html(response.content);
                    new Noty({
                        type: 'success',
                        layout: 'center',
                        theme : 'relax',
                        text: response.message
                    }).show();
                },
                error: function (jqXHR, exception) {

                },
            });
            return false;
        });
    </script>
@endsection
