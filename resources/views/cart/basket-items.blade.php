                            @foreach($items as $item)
                                <li class="basket-item">
                                    <div class="image">
                                        <a href="/products/{{$item->model->slug}}"><img src="{{$item->model->resized('150x150')[0]}}" alt="" class="img-responsive"></a>
                                    </div>
                                    <div class="text">
                                        <p class="title"><a href="/products/{{$item->model->slug}}">{{$item->model->title}}</a></p>
                                        <p class="subtitle">Сумка женская</p>
                                        <p class="count">{{$item->qty}} x <span class="sum">{{$item->price}} <i class="fa fa-rouble"></i></span></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            @endforeach    
                                <li class="button-item">
                                    <a href="/order" class="btn btn-default">ПЕРЕЙТИ К ОФОРМЛЕНИЮ</a>
                                    <a href="/cart" class="btn btn-primary">ПЕРЕЙТИ В КОРЗИНУ</a>
                                </li>