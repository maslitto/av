@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists() !!}
    <!--CART-->
    @if(Auth::guest())
        <h3 class="text-center" style="padding:200px 0"><a href="/login" style="border-bottom: 1px #333 dashed">Войдите</a> или <a href="/register"  style="border-bottom: 1px #333 dashed">зарегистрируйтесь</a> чтобы видеть цены и делать заказы</h3>
    @else
        <section class="cart">
            <h1 class="section-title">Корзина</h1>
            @if(count($items)>0)
            <h4>В КОРЗИНЕ <span id="cart-count">{{Cart::count()}} </span><span> ТОВАРОВ</span> на сумму <span class="total">{{Cart::subtotal(0,'',' ')}} <i class="fa fa-rouble"></span></i></h4>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2">Товар</th>
                            <th>Цвет</th>
                            <th>Артикул</th>
                            <th>Количество</th>
                            <th>Цена</th>
                            <th><i class="fa fa-trash"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                            <tr data-id="{{ $item->rowId }}" data-product_id="{{ $item->id }}">
                                <td style="text-align:center">
                                    <a href="/products/{{$item->model->slug}}" >
                                        <img src="{{$item->model->resized('150x150')[0]}}" alt="" class="img-responsive1">
                                    </a>
                                </td>
                                <td>
                                    <p class="title"><a href="#">{{$item->model->title}}</a></p>
                                    <p class="subtitle">{{$item->model->vid}} {{$item->model->gender}}</p>
                                </td>
                                <td>{{$item->model->color}}</td>
                                <td>{{$item->model->title}}</td>
                                <td>
                                    <!--a href="#" class="minus"><i class="fa fa-minus"></i></a-->
                                    <input type="number" value="{{$item->qty}}" name="count" class="count qty js-cart-qty" min-value="1" min="1">
                                    <!--a href="#" class="plus"><i class="fa fa-plus"></i></a-->
                                </td>
                                <td><span class="price">{{ number_format($item->model->price, 0, ',', ' ') }} <i class="fa fa-rouble"></i></span></td>
                                <td><a href="#" class="remove js-cart-remove"><img src="/template/img/icon-close.png" alt=""></a></td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" class="promo-block"> Промокод: <input type="text" class="promokod"></td>
                            <td colspan="2">без учета доставки</td>
                            <td colspan="2"> <span class="baseline">ИТОГО <span class="total">{{Cart::subtotal(0,'',' ')}} <i class="fa fa-rouble"></i></span></span></td>
                        </tr>
                        <tr class="buttons">
                            <td colspan="4"> <a href="/catalog" class="btn btn-grey"><i class="fa fa-arrow-left"></i> В магазин</a></td>
                            <td colspan="3" class="text-right"> <a href="/order" class="btn btn-primary">Оформить заказ <i class="fa fa-arrow-right"></i></a></td>
                        </tr>
                    </tfoot>
            </table>    
            </div>
            <div class="text-block">
                <p class="text-center">Если у Вас нет времени, чтобы оформить заказ на нашем сайте, Вы можете сделать это через оператора, <br>
просто позвонив по телефону <span class="pink">{{config('app.phone')}}</span> назвав артикул товара.</p>
            </div>
            <hr>

        </section>
        <section class="popular carousel">
            <h2 class="section-title">Добавьте к заказу</h2>
            
            <div class="items-carousel js-items-carousel">
                @foreach($recommends as $product)
                    @include('product.product')
                @endforeach
               
            </div>
           
        </section> 
        @else
            <div class="text-center">
                <h3>Ваша корзина пуста(</h3>
                <a href="/catalog" class="btn btn-primary"> Перейти в каталог</a>
            </div>
        @endif
    @endif

@endsection    

