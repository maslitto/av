@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists() !!}
    <!--CART-->
    <section class="news">
        <h1 class="section-title">Новости</h1>
        <div class="content">
            @foreach($news as $k=>$item)
                @if($k % 2 ==0)
                    <div class="row">
                        <div class="col-xs-6">
                            <img src="{{$item->image}}" alt="{{$item->title}}" class="img-responsive">
                        </div>
                        <div class="col-xs-6">
                            <p class="small date">{{$item->updated_at}}</p>
                            <p class="lead" >{{$item->title}}</p>
                            <div class="small">{!!$item->text!!}</div>
                        </div>
                    </div>
                    <hr>
                @else
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="small date">{{$item->updated_at}}</p>
                            <p class="lead" >{{$item->title}}</p>
                            <p class="small">{!!$item->text!!}</p>
                        </div>
                        <div class="col-xs-6">
                            <img src="{{$item->image}}" alt="{{$item->title}}" class="img-responsive">
                        </div>
                    </div>
                    <hr>
                @endif
            @endforeach
            <div class="text-center">{{$news->links()}}</div>
        </div>

    </section> 


@endsection    
@section('js')
    @parent
@endsection
