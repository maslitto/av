@extends('layouts.app')

@section('content')
    {!! Breadcrumbs::renderIfExists() !!}
    <section class="profile">
        <h2 class="section-title">Личный кабинет</h2>

        <nav class="text-center tab-selector">
            <ul class="" role="tablist">
                <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Профиль</a></li>
                <li role="presentation"><a href="#myorders" aria-controls="myorders" role="tab" data-toggle="tab">Мои заказы</a></li>
                <li role="presentation"><a href="#wishlist" aria-controls="wishlist" role="tab" data-toggle="tab">Избранное</a></li>
            </ul>
        </nav>
        <div class="content">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="profile">
                    <div class="header">
                        <h3>ПРОФИЛЬ</h3>
                    </div>
                    <div class="forms">
                        <div class="row">
                            <div class="col-xs-12">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <h3 class="text-center">При заполнении формы возникли ошибки!</h3>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-xs-3 text-center">
                                <img src="/images/blank.png" alt="" class="img-responsive" width="150">
                            </div>
                            <div class="col-xs-9">
                                <form action="/profile" method="post">
                                    {!! csrf_field() !!}
                                    <h3>ПЕРСОНАЛЬНЫЕ ДАННЫЕ</h3>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="radio radio-primary">
                                                    <input id="buyer1" class="styled" @if(!$user->opt) checked @endif name="opt" value="0" type="radio">
                                                    <label for="buyer1">
                                                        Я розничный покупатель
                                                    </label>
                                                </div>
                                                <div class="radio radio-primary">
                                                    <input id="buyer2" class="styled" @if($user->opt) checked @endif name="opt" value="1" type="radio">
                                                    <label for="buyer2">
                                                        Я оптовый покупатель
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>ИМЯ</label>
                                                <input name="name" class="form-control" required="" type="text" value="{{$user->name}}">
                                            </div>
                                            <div class="form-group">
                                                <label>E-mail</label>
                                                <input name="email" class="form-control" required="" type="email" disabled  value="{{$user->email}}">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Фамилия</label>
                                                <input name="second_name" class="form-control" required="" type="text"  value="{{$user->second_name}}">
                                            </div>
                                            <div class="form-group">
                                                <label>ТЕлефон</label>
                                                <input name="phone" class="form-control" required="" type="text"  value="{{$user->phone}}">
                                            </div>
                                        </div>
                                    </div>
                                    <h3>адрес доставки</h3>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Страна</label>
                                                <input name="country" class="form-control"  value="{{$user->country}}" type="text">
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label>Индекс</label>
                                                        <input name="index" class="form-control" type="text"  value="{{$user->index}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label>Улица</label>
                                                        <input name="street" class="form-control" type="text"  value="{{$user->street}}">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Город</label>
                                                <input name="city" class="form-control" type="text"  value="{{$user->city}}">
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label>Дом</label>
                                                        <input name="home" class="form-control" type="text"  value="{{$user->home}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label>Квартира</label>
                                                        <input name="apt" class="form-control" type="text"  value="{{$user->apt}}">
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <hr>
                                    <button type="submit" class="btn btn-default">СОХРАНИТЬ</button>
                                </form>
                                <hr>
                                <form action="/profile/changepassword" method="post">
                                    {!! csrf_field() !!}
                                    <h3>Смена пароля</h3>
                                    <div class="row">

                                        <div class="col-xs-7">
                                            <div class="form-group">
                                                <label>Текущий пароль</label>
                                                <input name="old_password" class="form-control" type="password">
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label>Новый пароль</label>
                                                        <input name="password" class="form-control" type="password">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <label>Повторите пароль</label>
                                                        <input name="password_confirmation" class="form-control" type="password">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        <button class="btn btn-default">Сменить пароль</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="wishlist">
                    <div class="header">
                        <h3>ИЗБРАННОЕ</h3>
                    </div>
                    @if(!empty($wishlist))
                        <div class="wishlist cart">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Товар</th>
                                            <th>Цвет</th>
                                            <th>Артикул</th>
                                            <th>Цена</th>
                                            <th></th>
                                            <th><i class="fa fa-trash"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($wishlist as $item)
                                        <tr data-pid="{{$item->id}}">
                                            <td>
                                                <img src="{{$item->resized('150x150')[0]}}" alt="" class="img-responsive">
                                            </td>
                                            <td>
                                                <p class="title"><a href="{{$item->url}}"></a></p>
                                                <p class="subtitle">{{$item->vid}} {{$item->gender}}</p>
                                            </td>
                                            <td>{{$item->color}}</td>
                                            <td>{{$item->title}}</td>

                                            <td><span class="price">{{$item->price}} <i class="fa fa-rouble"></i></span></td>
                                            <td>
                                                <a href="#" class="btn btn-default to-cart">В КОРЗИНУ</a>
                                            </td>
                                            <td><a href="#" class="delete"><img src="/template/img/icon-close.png" alt=""></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                    <tr class="buttons">
                                        <td colspan="4"> <a href="#" class="btn btn-grey destroy"><img src="/template/img/icon-close.png" alt="" width=13>  Удалить все</a></td>
                                        <td colspan="3" class="text-right"> <a href="#" class="btn btn-primary buy-all">Добавить все в корзину</a></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    @else
                        <div class="jumbotron">
                            <h1 class="text-center">В избранном пусто :(</h1>
                            <br><br>
                            <p class="text-center"><a class="btn btn-primary btn-lg" href="/catalog">Перейти в каталог</a></p>
                        </div>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane" id="myorders">
                    <div class="header">
                        <h3>МОИ ЗАКАЗЫ</h3>
                    </div>
                    @if(count($orders)>0)
                        <div class="orders table-responsive">
                            <table class="table" id="accordion">
                            <thead>
                            <tr>
                                <th>Дата:</th>
                                <th>Номер заказа:</th>
                                <th>Статус:</th>
                                <th>Сумма:</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr data-toggle="collapse" data-target="#accord{{$order->id}}" class="clickable collapsed" aria-expanded="false">
                                    <td>{{$order->created_at}}</td>
                                    <td>#{{$order->id}}</td>
                                    <td>{{$order->status}}</td>
                                    <td>{{$order->order_cost}} руб.</td>
                                </tr>
                                <tr class="hid" id="row{{$order->id}}">
                                    <td colspan="4">
                                        <div id="accord{{$order->id}}" class="collapse" aria-expanded="false">
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <h4 class="proximaNova-Bold">Информация о заказе:</h4>
                                                    <p>Номер заказа: <strong>#{{$order->id}}</strong></p>
                                                    <p>Дата размещения заказа: <strong>{{$order->created_at}}</strong></p>
                                                    <p>Доставка: <strong>{{$order->delivery_cost}} руб.</strong></p>
                                                </div>

                                                <div class="col-xs-5">
                                                    <h4 class="proximaNova-Bold">Информация о доставке:</h4>
                                                    <p>Ф.И.О: <strong>{{$order->name}} {{$order->second_name}}</strong></p>
                                                    <p>Город: <strong>{{$order->city}}</strong></p>
                                                    <p>Улица, дом, квартира: <strong>{{$order->street}},{{$order->home}},{{$order->apt}}</strong> </p>
                                                    <p>Индекс: <strong>{{$order->index}}</strong> </p>
                                                    <p>Телефон: <strong>{{$order->phone}}</strong></p>
                                                </div>
                                            </div>
                                            <table class="table table-inner">
                                                <thead>
                                                <tr>
                                                    <th>Товар:</th>
                                                    <th>Артикул:</th>
                                                    <th style="padding-left:80px;">Количество:</th>
                                                    <th>Сумма:</th>
                                                </tr>
                                                </thead>
                                                <tbody class="proximaNova-Bold">

                                                @foreach($order->products as $item)
                                                    <tr>
                                                        <td style="width: 40%;">{{$item->product->vid}} {{$item->product->gender}}</td>
                                                        <td>{{$item->product->title}}</td>
                                                        <td style="padding-left:80px;">{{$item->qty}}</td>
                                                        <td>{{$item->product->price}}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td></td>
                                                    <td style="vertical-align:middle"><a href="javascript:void(0);" class="pdf loadpdf" rel="137">PDF</a></td>
                                                    <td>
                                                        <p class="tahoma14 margin0">Всего товаров:</p>
                                                        <p class="tahoma14 margin0">Стоимость доставки: </p>
                                                        <p class="tahoma14 margin0">Всего (с учетом НДС):</p>
                                                    </td>
                                                    <td>
                                                        <p class="proximaNova-Bold margin0">{{count($order->products)}}</p>
                                                        <p class="proximaNova-Bold margin0">{{$order->delivery_cost}} руб.</p>
                                                        <p class="proximaNova-Bold margin0">{{$order->order_cost}} руб.</p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                        <div class="jumbotron">
                            <h1 class="text-center">У вас нет заказов</h1>
                            <h3 class="text-center">Оформите свой первый заказ прямо сейчас!</h3>
                            <br><br>
                            <p class="text-center"><a class="btn btn-primary btn-lg" href="/catalog">Перейти в каталог</a></p>
                        </div>
                    @endif
                </div>
            </div>


        </div>

    </section>
@endsection    
@section('js')
    @parent
    <script>
        function showEmpty(){
            $('.wishlist.cart').html('<div class="jumbotron">\
                <h1 class="text-center">В избранном пусто :(</h1>\
            <br><br>\
            <p class="text-center"><a class="btn btn-primary btn-lg" href="/catalog">Перейти в каталог</a></p>\
            </div>');
        }
        $('.to-cart').click(function() {
            btn = $(this);
            product = $(this).closest('tr');
            console.log(product.data('pid'));

            $.ajax({
                url: '{{route("cart.store")}}',
                type: "post",
                data: {
                    pid: product.data('pid'),
                    _token: token,
                },
                success: function (response) {
                    //alert(response.message);
                    new Noty({
                        type: 'success',
                        layout: 'center',
                        theme : 'relax',
                        text: response.message
                    }).show();
                    $('.basket .total').html(response.count);
                    $('#basket-cont').html(response.content);
                    $.ajax({
                        url: '{{ route('wishlist.store') }}',
                        type: "post",
                        data: {
                            pid: product.data('pid'),
                            _token: token,
                        },
                        success: function (response) {
                            if(response.count ==0) showEmpty();
                        }
                    });
                    product.remove();
                },
                error: function (jqXHR, exception) {

                },
            });

            return false;
        });
        $('.delete').click(function() {
            btn = $(this);
            product = $(this).closest('tr');
            console.log(product.data('pid'));
            $.ajax({
                url: '{{ route('wishlist.store') }}',
                type: "post",
                data: {
                    pid: product.data('pid'),
                    _token: token,
                },
                success: function (response) {
                    product.remove();
                    if(response.count ==0) showEmpty();
                },
                error: function (jqXHR, exception) {

                },
            });
            return false;
        });

        $('.destroy').click(function() {
            btn = $(this);
            $.ajax({
                url: '/wishlist/destroy',
                type: "post",
                data: {
                    _token: token,
                },
                success: function (response) {
                    shoEmpty();
                },
                error: function (jqXHR, exception) {

                },
            });
            return false;
        });
        $('.buy-all').click(function() {
            btn = $(this);
            $.ajax({
                url: '/wishlist/buyall',
                type: "post",
                data: {
                    _token: token,
                },
                success: function (response) {
                    showEmpty();
                    $('.basket .total').html(response.count);
                    $('#basket-cont').html(response.content);
                    new Noty({
                        type: 'success',
                        layout: 'center',
                        theme : 'relax',
                        text: response.message
                    }).show();
                },
                error: function (jqXHR, exception) {

                },
            });
            return false;
        });
    </script>
@endsection
