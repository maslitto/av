<!DOCTYPE html>
<html>
    <head>
        <title>ОШИБКА 404</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="/template/css/app.css">
        <style>

            html, body {
                height: 100%;
                background:#fff;
            }
            body{
                padding:100px;
            }



        </style>
    </head>
    <body>
        <div class="container ">
            <div class="content text-center">
                <p><a href="/" class="logo"><img src="/template/img/logo.png" alt=""></a></p>
                <div style="margin:50px 0"><img src="/template/img/404.jpg" alt="404"></div>
                <p style="margin:0 30%;text-align:center;font-family:'Roboto v15">Ошибка 404 означает, что страница, которую Вы
                    запрашиваете, не существует.
                    Возможно, она была удалена, возможно  Вы набрали неправильный адрес.
                </p>
                <p style="margin-top:50px"><a href="/" class="btn btn-primary">перейти на главную</a></p>
            </div>
        </div>
    </body>
</html>
