<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('guid')->unique();
            $table->string('sku');
            $table->string('title')->nullable();
            $table->integer('card')->nullable();
            $table->integer('show')->default(1);
            $table->string('slug')->nullable();

            $table->string('vid')->nullable();
            $table->string('season')->nullable();
            $table->string('gender')->nullable();
            $table->string('vidnomenklature')->nullable();
            $table->string('collection')->nullable();
            $table->string('material')->nullable();
            /*boolean options*/
            $table->integer('length')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();

            $table->string('color')->nullable();
            $table->string('description')->nullable();

            $table->json('images')->nullable();

            $table->text('content')->nullable();
            $table->text('metakeywords')->nullable();
            $table->text('metadescription')->nullable();


            /* foreign keys*/
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('pages');

            /*boolean options*/
            $table->integer('new')->nullable();
            $table->integer('req')->nullable();
            $table->integer('sale')->nullable();
            $table->integer('hit')->nullable();


            $table->integer('stock')->nullable();
            $table->integer('price')->nullable();
            $table->integer('price_opt')->nullable();
            $table->integer('price_sale')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
