<?php

use App\Model\Page;
use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $home = Page::create([
            'title'     => 'Главная',
            'slug'      => 'home',
        ]);
        $catalog = Page::create([
            'title'     => 'Каталог',
            'slug'      => 'catalog',
        ]);
        $women = Page::create([
            'title'     => 'Женские сумки',
            'slug'      => '',
        ]);
        $men = Page::create([
            'title'     => 'Мужские сумки',
            'slug'      => '',
        ]);
        $accs = Page::create([
            'title'     => 'Аксессуары',
            'slug'      => '',
        ]);
        $women->makeChildOf($catalog);
        $men->makeChildOf($catalog);
        $accs->makeChildOf($catalog);
    }
}
