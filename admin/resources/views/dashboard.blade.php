<!-- MAP & BOX PANE -->
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Административная панель</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <br>
                <p style="margin-left: 20px"><a href="/parser" class="btn btn-success btn-xl" target="_blank">Обновить товары</a></p>
                <br>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
</div>
