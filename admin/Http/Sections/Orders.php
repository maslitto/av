<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumnEditable;
use AdminSection;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Badge;
use SleepingOwl\Admin\Section;
use App\Model\Product;
use App\Model\Order;
use App\Model\OrderProduct;
use App\Model\OrderComment;

/**
 * Class Products
 *
 * @property \App\Model\Product $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Orders extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var strings
     */
    protected $title = 'Заказы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-shopping-cart');
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()->with('products', 'products')->with('user', 'user')->setApply(function($query) {
                $query->orderBy('created_at', 'desc');
            })->setColumns([
                AdminColumn::link('id', 'Номер'),
                AdminColumn::custom()->setLabel('Статус')->setCallback(function (Order $model) {
                    return $model->status ? ' Обработана' : 'Новая';
                }),
                AdminColumn::link('name')->setLabel('Имя'),
                AdminColumn::text('phone')->setLabel('Телефон'),
                //AdminColumnEditable::checkbox('new', 'Новинка'),
            ])->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::panel();
        $products = AdminSection::getModel(OrderProduct::class)->fireDisplay()->setApply(function($query) {
                $query->orderBy('created_at', 'asc');
            });
        $products->setApply(function($query) use($id) {
            $query->where('order_id', $id);
        });
        //$products->setParameter('order_id', $id);
        //$products->getColumns()->disableControls();

        $comments = AdminSection::getModel(OrderComment::class)->fireDisplay();
        $comments->setApply(function($query) use($id) {
            $query->where('order_id', $id);
        });
        $comments->setParameter('order_id', $id);
        $comments->getColumns()->disableControls();
          
        $tabs = AdminDisplay::tabbed([
            'Контакты' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('id', 'Номер заказа'),
                        AdminFormElement::select('status', 'Статус заявки')->setOptions([
                            0 => "Новая",
                            1 => "Обработана"
                        ]),
                        AdminFormElement::text('name', 'Имя'),
                        AdminFormElement::text('index', 'Индекс'),
                        AdminFormElement::text('country', 'Страна'),
                        AdminFormElement::text('city', 'Город'),
                        AdminFormElement::text('street', 'Улица'),
                        AdminFormElement::text('home', 'Дом'),
                        AdminFormElement::text('apt', 'Кв/офис'),
                        AdminFormElement::text('phone', 'Телефон'),
                        AdminFormElement::text('email', 'E-mail'),
                        AdminFormElement::text('delivery', 'Доставка'),
                        AdminFormElement::text('payment', 'Оплата'),
                        AdminFormElement::textarea('comment', 'Комментарий'),
                    ], 4)->addColumn([
                        
                        new \SleepingOwl\Admin\Form\FormElements([$products]),
                        new \SleepingOwl\Admin\Form\FormElements([$comments])
                    ])
            ])
        ]);

        $form->addElement($tabs);


        $form->getButtons()
            ->setSaveButtonText('Сохранить')
            ->hideSaveAndCloseButton();
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

}
