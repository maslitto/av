<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
/**
 * Class Blog
 *
 * @property \App\Model\Blog $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Blog extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var strings
     */
    protected $title = 'Блог';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-pencil');
    }

    public function onDisplay()
    {
        return AdminDisplay::table()->setApply(function($query) {
                $query->orderBy('created_at', 'desc');
            })->setColumns([
                AdminColumn::link('title', 'Заголовок'),
                AdminColumn::datetime('created_at', 'Дата публикации')->setFormat('d.m.Y')->setWidth('150px'),
                AdminColumnEditable::checkbox('published', 'Активный'),
            ])->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::radio('published', 'Статус публикации')->setOptions(['0' => 'Не опубликована', '1' => 'Опубликована'])->required(),
            AdminFormElement::text('title', 'Заголовок')->required(),
            AdminFormElement::image('image', 'Картинка')->required(),
            AdminFormElement::wysiwyg('text', 'Текст новости'),

            AdminFormElement::text('metatitle', 'Meta title')->required(),
            AdminFormElement::text('description', 'Meta description')->required(),
            AdminFormElement::text('keywords', 'Meta keywords')->required(),
            AdminFormElement::hidden('slug'),
            //AdminFormElement::date('date', 'Дата публикации')->required()->setFormat('d.m.Y'),
            
            
        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }
}
