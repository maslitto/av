<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumnEditable;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Badge;
use SleepingOwl\Admin\Section;
use App\Model\Product;

/**
 * Class Products
 *
 * @property \App\Model\Product $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Products extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Товары';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-shopping-bag');
    }
    /**
     * Переопределение метода для запрета удаления записи
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return bool
     */
    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return true;
    }
    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatablesAsync()->setApply(function($query) {
                $query->orderBy('created_at', 'desc');
            })->setColumns([
                AdminColumn::link('title', 'Заголовок'),
                AdminColumn::text('price')->setLabel('Цена'),
                
                //AdminColumnEditable::checkbox('new', 'Новинка'),
            ])->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $materials = Product::getDistinctParam('material');
        $form = AdminForm::form()->setElements([
                AdminFormElement::images('images', 'Галерея'),
                AdminFormElement::text('sku', 'Артикул')->required(),
                AdminFormElement::text('title', 'Заголовок')->required(),
                AdminFormElement::text('vid', 'Вид'),
                AdminFormElement::select('season', 'Сезон')->setOptions([
                    'Летний' => 'Летний',
                    'Зимний' => 'Зимний',
                ]),
                AdminFormElement::select('gender', 'Пол')->setOptions([
                    'Женский' => 'Женский',
                    'Мужской' => 'Мужской',
                ]),
                AdminFormElement::select('vidnomenklature', 'Вид номенкалтуры')->setOptions([
                    'Сумки' => 'Сумки',
                    'Шапки' => 'Шапки',
                    'Паланкины' => 'Паланкины',
                ]),
                AdminFormElement::select('material', 'Материал')->setOptions($materials),

        ]);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

}
