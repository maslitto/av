<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumnEditable;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Badge;
use SleepingOwl\Admin\Section;
use App\Model\Order;
use App\Model\OrderComment;

/**
 * Class Products
 *
 * @property \App\Model\Product $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class OrderComments extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration# ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Комментарии менеджера';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    
    public function initialize()
    {
        //$this->addToNavigation()->setIcon('fa fa-shopping-bag');
    }
    /**
     * Переопределение метода содержащего заголовок создания записи
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getCreateTitle()
    {
        return 'Добавить комментарий';
    }
    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::table()->setHtmlAttribute('class', 'table-primary table-responsive');
        //$display = AdminDisplay::table();
        $display->paginate(10);
        //$display->with('product', 'product');
     
        $display->setColumns([
            AdminColumn::text('id')->setLabel('#')
                ->setWidth('30px'),
            //AdminColumn::image('product.image')->setLabel('Картинка')->setWidth('20px'),
            //AdminColumn::text('product.title')->setLabel('Артикул'),
            //AdminColumn::text('qty')->setLabel('Количество'),
            AdminColumn::text('comment')->setLabel('Комментарий')
        ]);
        //$display->setOrder([[1, 'asc']]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::form()->setElements([
            AdminFormElement::textarea('comment', 'Комментарий')->required(),
            AdminFormElement::image('file', 'Файл'),
            AdminFormElement::hidden('order_id'),
        ]);

        $form->getButtons()
            ->setSaveButtonText('Сохранить')
            ->hideSaveAndCloseButton();
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

}
