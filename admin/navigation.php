<?php

/**
 * @var \SleepingOwl\Admin\Contracts\Navigation\NavigationInterface $navigation
 * @see http://sleepingowladmin.ru/docs/menu_configuration
 */

use SleepingOwl\Admin\Navigation\Page;

$navigation->setFromArray([

    [
        'title' => 'Права доступа',
        'icon' => 'fa fa-group',
        'priority' =>'10000',
        'pages' => [
            (new Page(\App\Model\User::class))
                ->setIcon('fa fa-user')
                ->setPriority(0),
            (new Page(\App\Model\Role::class))
                ->setIcon('fa fa-group')
                ->setPriority(100)
        ]
    ],
    [
        'icon' => 'fa fa-upload',
        'title' => 'Обновить товары',
        'url' => '/parser/'
    ],
]);
