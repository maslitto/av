<?php

return [

    'product' => [
    	['w' => 150 , 'h' => 150],
        ['w' => 300 , 'h' => 300],
        ['w' => 800 , 'h' => 800],
    ],

    'banner' => [
        ['w' => 1170 , 'h' => 390],
        ['w' => 320 , 'h' => 140],
    ],

];
