"use strict";

//= ../bower_components/jquery/dist/jquery.js

//= ../node_modules/bootstrap337/dist/js/bootstrap.js
//= ../bower_components/fancybox/source/jquery.fancybox.pack.js
//= ../node_modules/noty/lib/noty.js
//= ../node_modules/bootstrap-slider/dist/bootstrap-slider.js

//= vendor/slick.min.js
//= vendor/inputmask/inputmask.js
//= vendor/jquery.mlens-1.7.min.js


//= modules/sliders.js
//= modules/auth.js
//= modules/common.js
//= modules/catalog.js
//= modules/forms.js
//= modules/cart.js


var token = $('.js-token').attr('content');
var app = (function () {
	return {
		init: function () {
            cart.init();
            common.init();
			sliders.init();
			auth.init();
			catalog.init();
			forms.init();
		}
	}
})();
$(document).on('ready', app.init());



