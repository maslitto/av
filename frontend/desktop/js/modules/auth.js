var auth = (function () {
	return {
		init: function () {
			auth.login();
			auth.reset();
			auth.register();
		},

		login: function() {
            $('.js-login-form ').submit(function () {
                $(this).find('.errors').html('');
                $.ajax({
                    url: '/login',
                    type: "post",
                    data: $(this).serialize(),
                    success: function (response) {
                        location.reload();
                    },
                    error: function (response) {
                        $('.js-login-form .errors').html('<div class="alert alert-danger text-center" role="alert">Ошибка авторизации</div>');
                    }
                });
                return false;
            });
		},

        reset: function() {
            $('.js-reset-form ').submit(function () {
                $(this).find('.errors').html('');
                $.ajax({
                    url: '/password/email',
                    type: "post",
                    data: $(this).serialize(),
                    success: function (response) {
                        new Noty({
                            type: 'success',
                            layout: 'center',
                            theme: 'relax',
                            text: response.message
                        }).show();
                        $('header .collapse.in').collapse('hide')
                    },
                    error: function (response) {
                        if (response.status === 422) {
                            //process validation errors here.
                            var errors = response.responseText; //this will get the errors response data.
                            $erObj = jQuery.parseJSON(errors);
                            $errorsHtml = '<div class="alert alert-danger"><ul>';
                            $.each($erObj, function (key, value) {
                                $errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                            });
                            $errorsHtml += '</ul></div>';

                            $('.js-reset-form .errors').html($errorsHtml); //appending to a <div id="form-errors"></div> inside form
                        }
                        else {
                            var $errors = jQuery.parseJSON(response.responseText);
                            $('.js-reset-form .errors').html('<div class="alert alert-danger"><ul><li>' + $errors.message + '</li></ul></div>');
                        }

                    }
                });
                return false;
            });
        },

        register: function() {
            $('.js-register-form ').submit(function () {
                $(this).find('.errors').html('');
                $.ajax({
                    url: '/register',
                    type: "post",
                    data: $(this).serialize(),
                    success: function (response) {
                        new Noty({
                            type: 'success',
                            layout: 'center',
                            theme: 'relax',
                            text: response.message
                        }).show();
                        $('header .collapse.in').collapse('hide')
                    },
                    error: function (response) {
                        if (response.status === 422) {
                            //process validation errors here.
                            var errors = response.responseText; //this will get the errors response data.
                            $erObj = jQuery.parseJSON(errors);
                            $errorsHtml = '<div class="alert alert-danger"><ul>';
                            $.each($erObj, function (key, value) {
                                $errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                            });
                            $errorsHtml += '</ul></div>';

                            $('.js-register-form.errors').html($errorsHtml); //appending to a <div id="form-errors"></div> inside form
                        }

                    }
                });
                return false;
            });
        }
	}
})();