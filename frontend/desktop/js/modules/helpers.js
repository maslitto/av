var helpers = (function () {
	return {
		init: function(){
			helpers.format();
			helpers.cookieInit();
			helpers.resize();
		},
		setCookie: function (name, value, options) {
			options = options || {};
			var expires = options.expires;
			if (typeof expires == "number" && expires) {
				var d = new Date();
				d.setTime(d.getTime() + expires * 1000);
				expires = options.expires = d;
			}
			if (expires && expires.toUTCString) {
				options.expires = expires.toUTCString();
			}
			value = encodeURIComponent(value);
			var updatedCookie = name + "=" + value;
			for (var propName in options) {
				updatedCookie += "; " + propName;
				var propValue = options[propName];
				if (propValue !== true) {
					updatedCookie += "=" + propValue;
				}
			}
			document.cookie = updatedCookie;
		},
		cookieInit: function (  ){
			jQuery.cookie = function(e, i, o) {
				if ('undefined' == typeof i) {
					var n = null;
					if (document.cookie && '' != document.cookie)
						for (var r = document.cookie.split(';'), t = 0; t < r.length; t++) {
							var p = jQuery.trim(r[t]);
							if (p.substring(0, e.length + 1) == e + '=') {
								n = decodeURIComponent(p.substring(e.length + 1));
								break
							}
						}
					;return n
				}
				;o = o || {},
				null === i && (i = '',
					o.expires = -1);
				var u = '';
				if (o.expires && ('number' == typeof o.expires || o.expires.toUTCString)) {
					var s;
					'number' == typeof o.expires ? (s = new Date,
						s.setTime(s.getTime() + 24 * o.expires * 60 * 60 * 1e3)) : s = o.expires,
						u = '; expires=' + s.toUTCString()
				}
				;var a = o.path ? '; path=' + o.path : ''
					, c = o.domain ? '; domain=' + o.domain : ''
					, m = o.secure ? '; secure' : '';
				document.cookie = [e, '=', encodeURIComponent(i), u, a, c, m].join('')
			};
		},
		format: function () {
			Number.prototype.formatNum = function ( decPlaces, thouSeparator ){
				decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
				//decSeparator = decSeparator == undefined ? "." : decSeparator;
				var n = this;
				var sign = n < 0 ? "-" : "";
				var i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "";
				var j;
				j = (j = i.length) > 3 ? j % 3 : 0;
				return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator);
			};
		},
		// use: declOfNum(count, ['бонус', 'бонусов', 'бонуса']);
		decOfNum: function(number, titles) {
			var cases = [2, 0, 1, 1, 1, 2];
			number = Math.floor(number);
			return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
		},
		toClipboard: function($target) {
			console.log($target);
			if ($target && $target.length && $target instanceof jQuery ) {
				$target.select();
				try {
					document.execCommand('copy');
				} catch (err) {
					console.log(err);
				}
			}
		},
		decoratedPrice: function(price) {
			if(price == 0) {
				return 'бесплатно';
			}
			else {
				return price + ' ₽';
			}

		},
		resize: function() {
			var timer;
			$(window).resize(function() {
				clearTimeout(timer);
				timer = setTimeout(function(){
					$(document).trigger('windowResize');
				}, 300);
			});
		}
	}
})();