var sliders = (function () {
	return {
		init: function () {
		    //console.log('sliders init');
			sliders.mainPageSlider();
			sliders.productsCarousel();
			sliders.newsCarousel();
			sliders.selector();
		},

		mainPageSlider: function() {
            $('.js-banner').slick({
                dots: true
            });
		},

		productsCarousel: function () {
            $('.js-items-carousel').slick({
                infinite: true,
                dots:false,
                slidesToShow: 4,
                autoplay: false,
                slidesToScroll: 4,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        },

        newsCarousel: function () {
            $('.js-news-carousel').slick({
                infinite: true,
                dots:false,
                slidesToShow: 3,
                autoplay: false,
                slidesToScroll: 1,
                appendArrows: '.js-arrows',
                prevArrow:'<button type="button" class="slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
                nextArrow:'<button type="button" class="slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>',
            });
        },

        selector: function () {
            var filtered = false;
            $('.js-selector').click(function(){
                var data = $(this).data("filter");
                console.log(data);
                var section = $(this).closest('.carousel');
                if(data=="all"){
                    section.find('.js-items-carousel').slick('slickUnfilter');
                    filtered = false;
                }
                else{
                    section.find('.js-items-carousel').slick('slickUnfilter');
                    section.find('.js-items-carousel').slick('slickFilter','.'+data);
                    filtered = true;
                }
                $('.js-selector.active').removeClass('active');
                $(this).addClass('active');
                return false;
            });
        }
	}
})();