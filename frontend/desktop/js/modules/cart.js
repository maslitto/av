var cart = (function () {
	return {
		init: function () {
		    console.log('cart init');
		    cart.add();
			cart.changeQty();
			cart.remove();
			cart.clean();
		},
        add: function () {
            $(document).on('click', '.js-cart-add', function (event) {
                event.preventDefault();
                var $product = $(this).closest('.js-cart-add-form'),
                    count = parseInt($product.find('input[name=count]').val());
                $.ajax({
                    url: '/cart',
                    type: "post",
                    data: {
                        pid: $product.data('pid'),
                        _token: token,
                        count: count
                    },
                    success: function (response) {
                        new Noty({
                            type: 'success',
                            layout: 'center',
                            theme: 'relax',
                            timeout: 1000,
                            text: response.message
                        }).show();
                        $('.basket .total').html(response.count);
                        //$('#basket-cont').html(response.content);
                    },
                    error: function (jqXHR, exception) {

                    }
                });
                return false;
            });
        },

		changeQty: function() {
            $('.js-cart-qty').change(function(e){
                //e.preventDefault();
                var $item = $(this).closest("tr");
                $.ajax({
                    type: "put",
                    url: "/cart/"+$item.data('id'),
                    data: {qty : $(this).val(), _token: token,},
                    success: function(response) {
                        $('#cart-count').html(response.count);
                        $('.total').html(response.total+' <i class="fa fa-rouble">');
                    }
                });
            });
        },

        remove: function() {
            $('.js-cart-remove').click(function(e){
                e.preventDefault();
                var $item = $(this).closest("tr");
                $.ajax({
                    type: "delete",
                    url: "/cart/"+$item.data('id'),
                    data: {qty : $(this).val(), _token: token,},
                    success: function(response) {
                        $item.remove();
                        $('#cart-count').html(response.count);
                        $('.total').html(response.total+' <i class="fa fa-rouble">');
                        if(response.count==0){
                            $('.cart').html('<div class="text-center">\
                                            <h3>Ваша корзина пуста(</h3>\
                                            <a href="/catalog" class="btn btn-primary"> Перейти в каталог</a>\
                                        </div>'
                            );
                        }
                    }
                });
            })
        },

        clean: function () {
            $('.js-cart-clean').click(function(){
                var token = $("#token").attr("value");
                $.ajax({
                    type: "post",
                    url: "/cart/clean",
                    data: {_token: token},
                    success: function(response) {
                        location.reload();
                    }
                })
            })
        }
	}
})();