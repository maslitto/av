var sliders = (function () {
	return {
		init: function () {
			sliders.mainPageSlider();
			sliders.productsCarousel();
			sliders.newsCarousel();
		},
		mainPageSlider: function() {
            $('.js-banner ').slick({
                dots: true
            });
		},
		productsCarousel: function () {
            $('.js-items-carousel').slick({
                infinite: true,
                dots:false,
                slidesToShow: 4,
                autoplay: false,
                slidesToScroll: 4
            });
        },
        newsCarousel: function () {
            $('.js-news-carousel').slick({
                infinite: true,
                dots:false,
                slidesToShow: 3,
                autoplay: false,
                slidesToScroll: 1,
                appendArrows: '.js-arrows',
                prevArrow:'<button type="button" class="slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
                nextArrow:'<button type="button" class="slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>',
            });
        }

	}
})();