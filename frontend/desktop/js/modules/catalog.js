var catalog = (function () {
	return {
		init: function () {
			catalog.filters();
			catalog.sorting();
		},
		filters: function() {
            $(".js-catalog-show-filters").click(function (e) {
                e.preventDefault();
                $('.js-catalog-filter').toggleClass('hidden-xs');
            });
            $(".js-close-catalog-filter").click(function (e) {
                e.preventDefault();
                $('.js-catalog-filter').toggleClass('hidden-xs');
            });


        },
        sorting: function() {
            $(".js-catalog-show-sorting").click(function (e) {
                e.preventDefault();
                $('.js-catalog-sorting').toggleClass('hidden-xs');
            });
            $(".js-close-catalog-sorting").click(function (e) {
                e.preventDefault();
                $('.js-catalog-sorting').toggleClass('hidden-xs');
            });

        },

	}
})();