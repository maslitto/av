var common = (function () {
	return {
		init: function () {
		    common.craps();
			common.callback();
			common.subscribe();
			common.wishlist();
			common.fastview();
			common.lens();
		},
        craps: function () {
            $("[rel='tooltip']").tooltip();

            $(document).on("click", '.buy1click .btn', function (event) {
                event.preventDefault();
                $.ajax({
                    type: "get",
                    url: "/callback",
                    data: $(".buy1click form").serialize(),
                    success: function (response) {
                        $(".buy1click").collapse('toggle');
                        $('.buy1click form')[0].reset();
                        //alert(response);
                        new Noty({
                            type: 'success',
                            layout: 'center',
                            theme: 'relax',
                            timeout: 1000,
                            text: response
                        }).show();

                    },
                    error: function (response) {
                        var errors = $.parseJSON(response.responseText);
                        $.each(errors.errors, function (key, value) {
                            $('input[name=' + key + ']').addClass('error');
                        });
                    }
                });
            });
            $("input[name=robot]").val('fuck-u-robots');

            $('header [data-toggle="collapse"]').click(function () {
                $('header .collapse.in').collapse('hide')
            });
        },

        fastview: function () {
            $('.js-fastview').click(function (e) {
                e.preventDefault();
                var pid = $(this).data("pid");
                console.log(pid);
                $.ajax({
                    type: "get",
                    url: "/getbypid",
                    data: {
                        pid: pid
                    },
                    success: function (data) {
                        $.support.transition = false;
                        $("#modalproduct .modal-body").html(data);
                        //initCarousel();
                        $("#modalproduct").modal("show");
                        forms.createNumberType('#modalproduct');
                    }
                });
            });
        },

		callback: function() {
            $(".js-callback-btn").click(function () {
                $.ajax({
                    type: "get",
                    url: "/callback",
                    data: $(".js-callback-form").serialize(),
                    success: function (response) {
                        $(".js-callback").collapse('toggle');
                        $('.js-callback-form')[0].reset();

                        new Noty({
                            type: 'success',
                            layout: 'center',
                            theme: 'relax',
                            text: response
                        }).show();
                    },
                    error: function (response) {
                        var errors = $.parseJSON(response.responseText);
                        errorsHtml = '<div class="alert alert-danger"><ul>';
                        $.each(errors.errors, function (key, value) {
                            errorsHtml += '<li>' + value[0] + '</li>';
                        });
                        errorsHtml += '</ul></div>';

                        $('.js-callback .errors').html(errorsHtml);
                    }
                });
                return false;
            });

        },

		wishlist: function() {
            $(document).on("click", '.js-wishlist', function (event) {
                console.log('wish click');
                event.preventDefault();
                var $wish = $(this);
                var pid = $wish.data('pid');
                $.ajax({
                    url: '/wishlist',
                    type: "post",
                    data: {
                        pid: pid,
                        _token: token,
                    },
                    success: function (response) {
                        $('.wishlist-header .total').html(response.count);
                        $wish.toggleClass('active');
                    },
                    error: function (jqXHR, exception) {

                    },
                });
                return false;
            })
        },

		subscribe: function() {
            $('.subscription-form').submit(function () {
                $.ajax({
                    url: '/subscribe',
                    type: "post",
                    data: $(this).serialize(),
                    success: function (response) {
                        new Noty({
                            type: 'success',
                            layout: 'center',
                            theme: 'relax',
                            text: response.message
                        }).show();
                    },
                    error: function (response) {
                        console.log(response);
                        new Noty({
                            type: 'error',
                            layout: 'center',
                            theme: 'relax',
                            text: response.responseJSON.errors.email[0]
                        }).show();
                    }
                });
                return false;
            });
        },
        
        lens: function () {
            $(document).ready(function()
            {
                $(".js-lens").mlens(
                    {
                        imgSrc: $(".js-lens").attr("data-big"),	  // path of the hi-res version of the image
                        imgSrc2x: $(".js-lens").attr("data-big2x"),  // path of the hi-res @2x version of the image
                                                                  //for retina displays (optional)
                        lensShape: "square",                // shape of the lens (circle/square)
                        lensSize: ["30%","30%"],            // lens dimensions (in px or in % with respect to image dimensions)
                                                            // can be different for X and Y dimension
                        borderSize: 4,                  // size of the lens border (in px)
                        borderColor: "#e07384",            // color of the lens border (#hex)
                        borderRadius: 0,                // border radius (optional, only if the shape is square)
                        imgOverlay: $(".js-lens").attr("data-overlay"), // path of the overlay image (optional)
                        overlayAdapt: true,    // true if the overlay image has to adapt to the lens size (boolean)
                        zoomLevel: 1,          // zoom level multiplicator (number)
                        responsive: true       // true if mlens has to be responsive (boolean)
                    });
            });
        }
	}
})();