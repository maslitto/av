## Демо (небольшой интернет-магазин) на laravel 5.5

[![Build Status](https://travis-ci.org/SleepingOwlAdmin/demo.svg?branch=master)](https://travis-ci.org/SleepingOwlAdmin/demo)



## Установка

* After cloning the repository run `composer install`
* Create `.env` file from `.env.example` and replace database and other data to you inside
* Run `composer update`
* Run `php artisan key:generate`
* Run `php artisan migrate --seed`
* Run `php artisan update-products`

Enjoy!

Оригинал http://angelo-vani.ru