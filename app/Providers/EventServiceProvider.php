<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Model\Product;
use App\Model\Banner;
use App\Model\OrderComment;
use File;
use Image;
use Mail;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\CallBackRequestEvent' => [
            'App\Listeners\SendCallbackNotification',
        ],
        'App\Events\OrderStored' => [
            'App\Listeners\SetOrderProducts',
            'App\Listeners\SendOrderStoredUserNotification',
            'App\Listeners\SendOrderStoredManagerNotification',
        ],
        'App\Events\CreateSubscribeEvent' => [
            'App\Listeners\SendSubscribeConfimation',
        ],
        'App\Events\AutoRegisterUserEvent' => [
            'App\Listeners\SendAutoRegisteredUserEmail',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Product::created(function($product) {
            $product->createThumbnails();
        });
        Product::updated(function($product) {
            $product->createThumbnails();
        });
        Banner::created(function($banner) {
            $banner->createThumbnails();
        });
        Banner::updated(function($banner) {
            $banner->createThumbnails();
        });
        OrderComment::created(function($comment) {
            Mail::send(
            'emails.content', array('title' => 'Новый комментарий менеджера к заказу в интернет-магазине angelo-vani.ru', 'content' => $comment->comment ), function (\Illuminate\Mail\Message $message) use ($comment) {
                $message->subject('Новый комментарий к заказу #'.$comment->order->id.'  в интернет-магазине  angelo-vani.ru');
                $message->from(config('app.manager_mail'), config('app.name'));
                $message->to($comment->order->email, $comment->order->name);
                if(!empty($comment->file)){
                    $path = public_path($comment->file);
                    $message->attach($path);
                }
                
            });
        });
    }
}
