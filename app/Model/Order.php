<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\User;

class Order extends Model
{
    protected $guarded = [];

    //Relations

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->hasMany('App\Model\OrderProduct');
    }

    /**
     * Returns full address as string
     * @param $value
     * @return string
     */
    public function getAddressAttribute($value){
    	return $this->index.', '.$this->country.', '.$this->city.', '.$this->street.', '.$this->home.', '.$this->apt ;
    }
}
