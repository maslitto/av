<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Slug;

class Blog extends Model
{

    protected $table = 'blogs';

    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    }
    public function getUrlAttribute($value)
    {
        return 'blog/'.$this->attributes['slug'];
    }

    public function setSlugAttribute($value)
    {
        //set translit slug
        $this->attributes['slug'] = Slug::make($this->title);
    }
}
