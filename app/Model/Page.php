<?php namespace App\Model;

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Slug;
class Page extends Node
{
    use SoftDeletes;

    protected $table = 'pages';
    protected $dates = ['creted_at','updated_at','deleted_at'];
    /**
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Slug::make($this->title);
    }

    public function getSlugAttribute($value)
    {
        if($this->id == 1){
            return '';
        } else
        return $value;
    }
}
