<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Slug;
use Auth;
use File;
use Image;

class Product extends Model
{
    use HasTags;

    const NUM_ITEMS = 24;

    protected $guarded = ['id'];
    protected $table = 'products';
    protected $casts = [
        'images' => 'array',
    ];

    /**
     * This function get distinct selected parameter from table
     * @param $param
     * @param \App\Model\Page|null $page
     * @return array
     */
    public static function getDistinctParam($param, $page = null){
        if(isset($request->slug)){
            $products = self::active()->where('category_id',$page->id)->distinct()->get([$param]);
        } else{
            $products = self::active()->distinct()->get([$param]);;
        }
        $values = [];
        foreach($products as $product){
            if(!empty($product->$param)){
                $values[] = $product->$param;
            }
        }
        return $values;

    }

    /**
     * Returns resized images
     * @param $value
     * @return array
     */
    public function resized($value)
    {
        $images = $this->getBaseImagesAttribute(NULL);
        if(count($images)>0){
            foreach($images as $k=>$image) {
                $images[$k] = 'images/resized/'.$value.'/'.$image;
            }
            return $images;
        } else return ['https://dummyimage.com/300x300/ffffff/e07383.jpg&text=НЕТ+ФОТО'];
    }

    public function createThumbnails()
    {
        if(is_array($this->images)&&sizeOf($this->images) > 0){
            foreach(config('images.product') as $size){
                $folderPath = public_path('images/resized/'.$size['w'].'x'.$size['h']);
                File::isDirectory($folderPath) or File::makeDirectory($folderPath, 0777, true, true);
                if(count($this->images)){
                    foreach($this->images as $image){
                        if (!file_exists($folderPath.basename($image))) {
                            $background = Image::canvas($size['w'], $size['h'],'#fff');
                            if(file_exists(public_path().'/'.$image)){
                                $resized = Image::make(public_path().'/'.$image)->resize($size['w'], $size['h'], function ($c) {
                                    $c->aspectRatio();
                                    $c->upsize();
                                });
                                $background->insert($resized, 'center');
                                $background->save($folderPath.'/'.basename($image));
                            }
                        }
                    }
                }

            }

        }
    }

    //Relations
    public function category()
    {
        return $this->belongsTo(Page::class);
    }      

    //Scopes
    public function scopeActive($query)
    {
        return $query/*->where('card','>',0)*/->where('show',1)/*->whereNotNull('images')/*->where('stock','>',0)*/;
    }

    public function scopeMen($query)
    {
        return $query->where('gender','Мужская');
    }

    public function scopeWomen($query)
    {
        return $query->where('gender','Женская');
    }

    //Mutators & Accessors

    public function getImageAttribute($value)
    {
        if(isset($this->images)){
            if(count($this->images)>0)
                return $this->images[0];
        }
        else{
            return NULL;
        }

    }

    public function getDescriptionAttribute($value)
    {
        if(empty($value))
            return 'Сумка '.$this->title.' от производителя Angelo Vani в интернет-магазине. Доставка в регионы.';
        else 
            return $value;

    }

	public function getKeywordsAttribute($value)
    {
        return 'Сумки оптом, Angelo Vani, купить сумку,'.$this->title;
    }

    public function getMetaTitleAttribute($value)
    {
       return 'Купить сумку '.$this->title. ' от производителя Angelo Vani в интернет-магазине';

    }

    public function getBaseImagesAttribute($value)
    {
        $baseImages = [];
        if(isset($this->images)) {
            foreach ($this->images as $image) {
                $baseImages[] = basename($image);
            }
        }
        return $baseImages;
    }

    public function setSlugAttribute($value)
    {
        //set translit slug
        $this->attributes['slug'] = Slug::make($this->title);
    }

    public function getUrlAttribute($value)
    {
        return 'products/'.$this->attributes['slug'];
    }

    public function getPriceAttribute($value)
    {
        return $this->attributes['price_opt'];
    }
}

