<?php

namespace App\Model;

trait HasTags
{

    /**
     * A user may have multiple roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Assign the given role to the user.
     *
     * @param  string $role
     * @return mixed
     */
    public function assignTag(Tag $tag)
    {
        return $this->tags()->save($tag);
    }

    /**
     * Determine if the user has the given role.
     *
     * @param  mixed $role
     * @return boolean
     */
    public function hasTag($tag)
    {
        if (is_string($tag->title)) {
            return $this->tags->contains('title', $tag->title);
        }

        //return !! $tag->intersect($this->tags)->count();
    }
}
