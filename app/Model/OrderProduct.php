<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    //
    public function order(){
        return $this->belongsTo('App\Model\Order');
    }
    
    public function product(){
        return $this->hasOne('App\Model\Product','id','product_id');
    }

    /**
     * Returns price of product position
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->product->price * $this->qty;
    }
}
