<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use File;
use Image;

class Banner extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * Creates thumbnails with given size. Array of sizes described in images config.
     */
    public function createThumbnails()
    {
        if(!empty($image = $this->image)){
            foreach(config('images.banner') as $size){
                $path = public_path('images/resized/'.$size['w'].'x'.$size['h']);
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                if (!file_exists($path.basename($image))) {
                    $resized  = Image::make(public_path().'/'.$image)->fit($size['w'], $size['h']);
                    $resized->save($path.'/'.basename($image));
                }
            }

        }
    }
    public function getBaseImageAttribute($value)
    {
        if(isset($this->image)) {
            return basename($this->image);
        }
        else return;
    }

    public function resized($value)
    {
        $image = basename($this->image);
        return  'images/resized/'.$value.'/'.$image;
    }
}
