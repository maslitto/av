<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderComment extends Model
{
    //
    public function order(){
        return $this->belongsTo('App\Model\Order');
    }
}
