<?php namespace App\Model;

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Slug;

class Tag extends Node
{
    const TYPE_VID = 1;
    const TYPE_MATERIAL = 2;
    const TYPE_MIXED = 3;

    protected $table = 'tags';
    /**
     * A tag may have multiple products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
    public function scopeActive($query)
    {
        return $query->whereHas('products', function ($q) {
            $q->active();
        });
    }
    /**
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Slug::make($this->title);
    }

}
