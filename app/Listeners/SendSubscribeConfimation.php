<?php

namespace App\Listeners;

use App\Events\CreateSubscribeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendSubscribeConfimation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateSubscribeEvent  $event
     * @return void
     */
    public function handle(CreateSubscribeEvent $event)
    {
        $confirmSubscription = $event->confirmSubscription;
        Mail::send('emails.confirm_subscribe',['token'=>$event->confirmSubscription->token],function($u) use ($confirmSubscription){
            $u->from(config('app.manager_mail'));
            $u->to($confirmSubscription->email);
            $u->subject('Подтверждение подписки на сайте angelo-vani.ru');
        });
    }
}
