<?php

namespace App\Listeners;

use App\Events\AutoRegisterUserEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendAutoRegisteredUserEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AutoRegisterUserEvent  $event
     * @return void
     */
    public function handle(AutoRegisterUserEvent $event)
    {
        $user = $event->user;
        $password = $event->password;
        Mail::send('emails.auto_register',['user'=>$user,'password' => $password],function($u) use ($user){
            $u->from(config('app.manager_mail'));
            $u->to($user->email);
            $u->subject('Регистрация на сайте '.config('app.name'));
        });
    }
}
