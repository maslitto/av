<?php

namespace App\Listeners;

use App\Events\OrderStored;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Model\OrderProduct;
use Cart;

class SetOrderProducts
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderStored  $event
     * @return void
     */
    public function handle(OrderStored $event)
    {
        $cart = $event->cart;
        $order = $event->order;
        if(!empty($cart)){
            foreach($cart as $item){
                $orderProduct = new OrderProduct();
                $orderProduct->product_id = $item->id;
                $orderProduct->qty = $item->qty;
                $order->products()->save($orderProduct);
            }
            $order->save();
        }
        Cart::destroy();

    }
}
