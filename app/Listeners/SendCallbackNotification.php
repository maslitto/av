<?php

namespace App\Listeners;

use App\Events\CallBackRequestEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Services\SmsService;

class SendCallbackNotification
{
    private $smsService;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SmsService $smsService )
    {
        $this->smsService = $smsService;
    }

    /**
     * Handle the event.
     *
     * @param  CallBackRequestEvent  $event
     * @return void
     */
    public function handle(CallBackRequestEvent $event)
    {
        $callback = $event->callback;
        Mail::send(
            'emails.callback', array('name' => $callback->name, 'phone' => $callback->phone), function (\Illuminate\Mail\Message $message) {
            $message->subject('Заказ звонка на angelo-vani.ru');
            $message->from(config('app.manager_mail'), config('app.name'));
            $message->to(config('app.manager_mail'), config('app.name'));
        });
        $sms = 'Заказ звонка Angelo-Vani / Имя: ' . $callback->name . ' Тел.: ' . $callback->phone;
        $response = $this->smsService->send($sms);

    }
}
