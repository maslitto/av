<?php

namespace App\Listeners;

use App\Events\OrderStored;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendOrderStoredManagerNotification
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderStored  $event
     * @return void
     */
    public function handle(OrderStored $event)
    {
        //
        $order = $event->order;
        Mail::send(
            'emails.order', array('order' => $order), function (\Illuminate\Mail\Message $message) use ($order) {
            $message->subject('Новый заказ на сайте angelo-vani.ru');
            $message->from(config('app.manager_mail'), config('app.name'));
            $message->to(config('app.manager_mail'), $order->name);
        });
    }
}
