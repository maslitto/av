<?php
/**
 * Created by PhpStorm.
 * User: veteran
 * Date: 06.10.18
 * Time: 14:29
 */

namespace App\Services;

use Ixudra\Curl\Facades\Curl;

class MorpherService
{
    const MORPHER_URL = 'http://ws3.morpher.ru/russian/declension'; //?format=json&s=
    const MORPHOS_URL = 'http://morphos.io/api/inflect-name'; //?_format=json&name=

    public function morph(string $word)
    {
        $response = Curl::to(self::MORPHOS_URL)
            ->withData( array( '_format' => 'json','name' => $word ) )
            ->asJson()
            ->get();
        //dd($response);
        /*$json = json_decode(file_get_contents(urlencode(self::MORPHER_URL.$word)));
        return $json;*/
        return $response;
    }

}