<?php
/**
 * Created by PhpStorm.
 * User: maslitto
 * Date: 04.10.18
 * Time: 13:51
 */

namespace App\Services;

use App\Model\Product;
use App\Model\OrderProduct;
use App\Model\Page;
use App\Model\Tag;

class ParserService
{
    protected $json = "ftpuser/nom.json";
    protected $photos = "ftpuser/";
    private $morpher;

    public function __construct()
    {
        $this->morpher = new MorpherService();
    }

    public static function deleteFiles(string $str) {
        //It it's a file.
        if (is_file($str)) {
            //Attempt to delete it.
            return unlink($str);
        }
        //If it's a directory.
        elseif (is_dir($str)) {
            //Get a list of the files in this directory.
            $scan = glob(rtrim($str,'/').'/*');
            //Loop through the list of files.
            foreach($scan as $index => $path) {
                //Call our recursive function.
                self::deleteFiles($path);
            }
            //Remove the directory itself.
            return @rmdir($str);
        }
    }

    private function truncateTags(){
        \DB::query('delete FROM `tags` where parent_id > 0');
        \DB::table('product_tag')->truncate();

    }
    
    public function parse()
    {
        ini_set('max_execution_time', 3600);
        $this->truncateTags();
        $olds = Product::all();
        foreach($olds as $old){
            $order = OrderProduct::where('product_id',$old->id)->first();
            if(!$order){
                $old->delete();
            }
            else{
                $old->show = 0;
                $old->save();
            }

        }
        $string = file_get_contents($this->json);
        $products = json_decode($string, false,3);
        $count = 0;
        foreach($products as $k => $product){
            if(empty($product->collection)||empty($product->Card)||$product->Card==''/*||empty($product->gender)*/) continue;

            $exist = Product::where('guid',$product->GUID)->first();
            if(!$exist){
                $new = new Product();
            }
            else{
                $new = $exist;
            }

            $new->guid = $product->GUID;
            $new->title = $product->FullName;
            $new->sku = $product->sku;
            $new->card = $product->Card;
            $new->vid = $product->vid;
            $new->gender = $product->gender;
            if($product->gender == 'Женская'){
                $category = Page::where('title','Женские сумки')->first();
            }
            elseif($product->gender == 'Мужская'){
                $category = Page::where('title','Мужские сумки')->first();
            }

            $new->collection = $product->collection;
            if(($product->sale == 'Да')||(!empty($product->PriceSale))) $product->collection = 'Распродажа';
            $collection = Page::where('title',$product->collection)->where('parent_id',$category->id)->first();
            if(!$collection){
                $collection = new Page();
                $collection->title = $product->collection;
                $collection->save();
                $collection->makeChildOf($category);
            }
            $new->season = $product->season;
            $new->vidnomenklature = $product->VidNomenklature;

            $new->material = $product->material;
            $new->width = $product->width;
            $new->height = $product->height;
            $new->length = $product->length;
            $new->color = $product->color;
            $new->description = $product->description;
            $product->new == 'Да' ? $new->new = 1 : $new->new = 0;
            $product->hit == 'Да' ? $new->hit = 1 : $new->hit = 0;
            $product->req == 'Да' ? $new->req = 1 : $new->req = 0;
            $product->sale == 'Да' ? $new->sale = 1 : $new->sale = 0;
            $new->stock = $product->count;

            $new->price_sale = preg_replace('/[^0-9]/', '', $product->PriceSale);
            if($product->PriceSale>0){
                $new->price_opt = preg_replace('/[^0-9]/', '', $product->PriceSale);
                $new->price = preg_replace('/[^0-9]/', '', $product->PriceSale);
                $new->old_price = preg_replace('/[^0-9]/', '', $product->PriceRoznica);
            }
            else{
                $new->price_opt = preg_replace('/[^0-9]/', '', $product->PriceOpt);
                $new->price = preg_replace('/[^0-9]/', '', $product->PriceRoznica);
            }

            $new->category_id = $collection->id;
            $new->slug = '';
            $new->save();
            //if($new->card){
                if(!empty($product->vid)){
                    $tag = $this->createTagIfNotExists($product->vid,'По типу',$new);
                    if(!$new->hasTag($tag)) $new->assignTag($tag);
                }
                if(!empty($product->material)) {
                    if($product->material == 'Текстиль'){
                        $material = ' из текстиля';
                    } elseif($product->material == 'Натуральная замша-искусственная кожа') {
                        $material = 'из '. 'натуральной замши-искусственной кожи';
                    } elseif($product->material == 'Лак') {
                        $material = 'лаковая';
                    } else{
                        $material ='из '. mb_strtolower($this->morpher->morph($product->material)->cases[1]);/////
                    }
                    $tag = $this->createTagIfNotExists($material, 'По материалу',$new);
                    if (!$new->hasTag($tag)) $new->assignTag($tag);
                }
                if(!empty($product->vid) && !empty($product->material)){
                    $vid = $product->vid;
                    if($product->material == 'Текстиль'){
                        $tagName = $vid.' из текстиля';
                    } elseif($product->material == 'Натуральная замша-искусственная кожа') {
                        $tagName = $vid.' из '. 'натуральной замши-искусственной кожи';
                    } elseif($product->material == 'Лак') {
                        $tagName = $vid.' лаковая';
                    } else{
                        $material = mb_strtolower($this->morpher->morph($product->material)->cases[1]);/////
                        $tagName = $vid.' из '. $material;
                    }
                    $tag = $this->createTagIfNotExists($tagName,'Смешанные',$new);
                    if(!$new->hasTag($tag)) $new->assignTag($tag);
                }
            //}
            $count++;
        }
        $this->updateImages($this->photos.'Card/');
        $this->updateImages($this->photos);
        Page::rebuild(true);
        return $count;
    }

    private function updateImages($path){
        set_time_limit(0);
        $images = glob($path . "*.*");

        //print_r($images);
        foreach($images as $image){
            $info = pathinfo($image);
            $product = Product::where('guid',$info['filename'])->first();
            if($product){
                //print_r(sizeOf($product->images));
                /*if(is_array($product->images)){
                    //die('array');
                    //die(sizeOf($product->images));
                        //die($image);
                        $product->images = [$image];
                        $product->save();

                } else{

                }*/
                $product->images = [$image];
                $product->save();

            }

        }
    }

    private function createTagIfNotExists(string $title,string $type, Product $product)
    {
        $tag = Tag::where('title', $title)->first();
        if(!$tag){
            $tag = new Tag();
        } else{
            return $tag;
        }
        $parent = Tag::where('title', $type)->first();
        $tag->title = $title;
        $topCategories = Page::where('parent_id', 2)->get();
        foreach($topCategories as $topCategory){
            if($product->category->isDescendantOf($topCategory)){
                $categoryName = $topCategory->title;
            }
        }
        $tag->metatitle = 'Купить '.mb_strtolower($categoryName).' '.$title. ' оптом';
        $tag->metadescription = 'Купить '.mb_strtolower($categoryName).' '.$title. ' оптом c доставкой по России.';
        $tag->metakeywords = mb_strtolower($categoryName).', оптом,'.$product->vid.','.$product->material;
        $tag->parent_id = $parent->id;
        $tag->slug = '';
        $tag->save();
        return $tag;
    }
}