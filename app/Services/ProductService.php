<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 22.12.2017
 * Time: 22:10
 */

namespace App\Services;

use App\Model\Page;
use App\Model\Product;
use App\Model\Tag;

/**
 * Service class for Product model;
 *
 * Class ProductService
 * @package App\Services
 */

class ProductService
{
    /**
     * This function get sale products;
     * @param int $limit
     * @return array
     */
    public function getSaleProducts(int $limit)
    {
        $sales =[];
        $women = Product::active()->women()->where('price_sale','>',0)->limit($limit)->get();
        $men = Product::active()->men()->where('price_sale','>',0)->limit($limit)->get();
        foreach($women as $item){
            array_push($sales,$item);
        }
        foreach($men as $item){
            array_push($sales,$item);
        }
        return $sales;
    }

    /**
     * This function get new products;
     * @param int $limit
     * @return array
     */
    public function getNewProducts(int $limit)
    {
        $new =[];
        $women = Product::active()->women()->where('new',1)->take($limit)->get();
        $men = Product::active()->men()->where('new',1)->take($limit)->get();

        foreach($women as $item){
            array_push($new,$item);
        }
        foreach($men as $item){
            array_push($new,$item);
        }
        return $new;
    }

    /**
     * Filter products in catalog
     * @param array $params
     * @param $page
     * @return object
     */
    public function getProductsQuery(array $params,$page)
    {
        $products = Product::active();
        if($page instanceof Page){
            $parents = $this->getParentsIds($page);
            $products = $products->whereIn('category_id',$parents);

        }
        if($page instanceof Tag){
            $ids = $this->getProductsIdsByTag($page);
            $products = $products->whereIn('id',$ids);
        }

        if(isset($params['lists'])){
            foreach($params['lists'] as $key => $value){
                $products = $products->whereIn($key,$value);
            }
        }
        if(isset($params['stock'])){
            if($params['stock']=='no'){
                $products->where('stock','=',0);
            } else{
                $products->where('stock','>',0);
            }
        }
        if(isset($params['new'])){
            $products = $products->where('new',1);
        }
        if(isset($params['hit'])){
            $products = $products->where('hit',1);
        }
        if(isset($params['sale'])){
            $products = $products->where('sale',1);
        }
        if(isset($params['priceFrom']) && isset($params['priceTo'])){
            $products = $products->where('price_opt','>=',$params['priceFrom'])->where('price_opt','<=',$params['priceTo']);
        }
        if(!empty($params['sortby'])){
            $products = $products->orderBy($params['sortby'],$params['sortdir']);
        } else{
            $products = $products->orderBy('stock','DESC');
        }
        /*$total = count($products->get());
        if(isset($params['per_page'])){
            $products = $products->paginate($params['per_page']);
        } else{
            $products = $products->paginate(Product::NUM_ITEMS);
        }*/
        /*$filter = (object)[
            'products' => $products,
            'priceMin' => $priceMin,
            'priceMax' => $priceMax,
            'total' => $total
        ];
        return $filter;*/

        return $products;
    }

    /**
     * This function get next product to current
     * @param int $id
     * @return mixed
     */
    public function getNextProduct(int $id)
    {
        $next = Product::active()->where('id','>',$id)->first();
        if(!$next){
            $next = Product::firstOrFail();
        }
        return $next;
    }
    /**
     * This function get next product to current
     * @param int $id
     * @return mixed
     */
    public function getPrevProduct(int $id)
    {
        $prev = Product::active()->where('id','<',$id)->first();
        if(!$prev){
            $prev = Product::latest()->firstOrFail();
        }
        return $prev;
    }
    /**
     * @param $page
     * @return array
     */
    private function getParentsIds($page)
    {
        $parents = [];
        if(!$page->isLeaf()){
            $leaves = $page->getLeaves();
            foreach($leaves as $leaf){
                $parents[] = $leaf->id;
            }
        }
        return $parents;
    }

    private function getProductsIdsByTag(Tag $tag){
        $products = $tag->products;
        foreach($products as $product){
            $ids[] = $product->id;
        }
        return $ids;
    }
    /**
     * Very simple search function
     * TODO ElasticSearch
     * @param $q
     * @return mixed
     */
    public function search($q)
    {
        $products = Product::active()
            ->where('title','LIKE','%'.$q.'%')
            ->orWhere('sku','LIKE','%'.$q.'%')
            ->orWhere('description','LIKE','%'.$q.'%')
            ->paginate(12);
        return $products;
    }


    /**
     * This function push product to session (last viewed products)
     * @param $product
     * @return array
     */

    public function pushToLastviews($product)
    {
        $lastviews = [];
        if(session()->has('lastviews')){
            $lastviews = session()->get('lastviews');
            if(!in_array($product->id,$lastviews)){
                session()->push('lastviews', $product->id);
            }
        } else{
            session()->push('lastviews', $product->id);
        }
        return $lastviews;
    }

    /**
     * Returns last viewed products from session
     * @return array
     */

    public function getLastViews()
    {
        $lastviews = [];
        if(session()->has('lastviews')){
            $ids = session()->get('lastviews');
            $lastviews = Product::whereIn('id', $ids)->limit(15)->get();
        }
        return $lastviews;
    }

    public function getFilters($page){
        $filters = [
            'materials' => Product::getDistinctParam('material',$page),
            'colors' => Product::getDistinctParam('color',$page),
            'seasons' => Product::getDistinctParam('season',$page),
            'collections' =>[],// Product::getDistinctParam('collection',$page),
            'vidnomenklatures' => Product::getDistinctParam('vidnomenklature',$page),
            'genders' => Product::getDistinctParam('gender',$page),
            'vids' => Product::getDistinctParam('vid',$page),
            'priceMin' => Product::active()->min('price_opt'),
            'priceMax' =>Product::active()->max('price_opt'),

        ];
        return $filters;
    }

}