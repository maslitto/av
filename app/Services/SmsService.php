<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 25.12.2017
 * Time: 0:08
 */

namespace App\Services;

class SmsService
{

    public function __construct()
    {
        //
    }

    /**
     * @param array $text
     * @return bool|string
     */
    public function send(string $text)
    {
        $body = file_get_contents("https://sms.ru/sms/send?api_id=" .env('SMS_API_KEY')."&to=". env('MANAGER_PHONE') . "&msg=" . urlencode($text) . "&json=1"); # Если приходят крякозябры, то уберите iconv и оставьте только urlencode("Привет!")
        return $body;
    }
}