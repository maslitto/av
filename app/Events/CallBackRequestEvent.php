<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Model\Callback;

class CallBackRequestEvent
{
    use SerializesModels;

    public $callback;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Callback $callback)
    {
        //
        $this->callback = $callback;
    }

}
