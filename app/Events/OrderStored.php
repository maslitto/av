<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Model\Order;
use Illuminate\Support\Collection;
use PhpParser\ErrorHandler\Collecting;

class OrderStored
{
    public $order;
    public $cart;

    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Order $order,Collection $cart)
    {
        //
        $this->order = $order;
        $this->cart = $cart;
    }

}
