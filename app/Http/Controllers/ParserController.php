<?php

namespace App\Http\Controllers;
use App\Model\Product;
use App\Model\OrderProduct;
use App\Model\Page;
use App\Services\MorpherService;
use App\Services\ParserService;

class ParserController extends Controller
{
    private $parserService;

    public function __construct()
    {
        $this->parserService = new ParserService();
    }

    public function parse()
    {
        $count = $this->parserService->parse();
        print_r('Успех! Загружено : '.$count . ' товаров');

    }
    
}
