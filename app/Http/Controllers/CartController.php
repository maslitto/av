<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Page;
use App\Model\Product;
use Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Cart::content();
        $recommends = Product::active()->limit(15)->get();
        return view('cart.cart',[
            'items' => $items,
            'recommends' => $recommends
        ]) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $product = Product::where('id',$request->pid)->firstOrFail();
        $qty = $request->get('count',1);
        $cartItem = Cart::add($product->id, $product->title, $qty, $product->price,[]);
        Cart::associate($cartItem->rowId, 'App\Model\Product');
        $response = [
            'success' => true,
            'message' => "В корзину добавлен товар:<br>$product->title x $qty шт.",
            'count' => Cart::count(),
            'product' => $product,
            'content' => view('cart.basket-items',['items' => Cart::content()])->render(),
        ];
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rowId)
    {
        //
        $qty = $request->qty;
        Cart::update($rowId, $qty);
        $response = [
            'total' => Cart::subtotal(0,'',' '),
            'count' => Cart::count(),
        ];
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rowId)
    {
        //
        Cart::remove($rowId);
        $response = [
            'total' => Cart::subtotal(0,'',' '),
            'count' => Cart::count(),
        ];
        return response()->json($response);
    }

    /**
     * Delete all items from cart
     *
     * @return \Illuminate\Http\Response
     */
    public function clean()
    {
        //
        Cart::destroy();
        $response = [
            'total' => Cart::subtotal(0,'',' '),
            'count' => Cart::count(),
        ];
        return response()->json($response);
    }
}
