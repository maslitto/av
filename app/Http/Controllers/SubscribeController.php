<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 14.04.2017
 * Time: 13:28
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;
use App\Model\ConfirmSubscription;
use App\Model\User;
use App\Http\Requests\CreateSubscribe;
use App\Events\CreateSubscribeEvent;
use App\Events\AutoRegisterUserEvent;

class SubscribeController extends Controller
{
    /**
     * @param CreateSubscribe $request
     * @return array
     */
    public function subscribe(CreateSubscribe $request)
    {

        $token = str_random(32);
        $confirmSubscription = new ConfirmSubscription;
        $confirmSubscription->token = $token;
        $confirmSubscription->email = $request->email;
        $confirmSubscription->save();

        event(new CreateSubscribeEvent($confirmSubscription));

        $response = [
            'message' => 'Проверьте почту для подтверждения регистрации'
        ];
        return $response;
    }
    /**
     * Confirm registration.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function confirm($token){
        $confirmSubscription = ConfirmSubscription::where('token','=',$token)->firstOrFail();
        $user = User::where('email','=',$confirmSubscription->email)->first();
        if($user){
            $user->subscribe = 1;
            $user->save();
        } else{
            $password = str_random(6);
            $user = new User();
            $user->email = $confirmSubscription->email;
            $user->password = $password;
            $user->subscribe = 1;
            $user->active = 1;
            $user->save();
            event(new AutoRegisterUserEvent($user,$password));
        }
        $confirmSubscription->delete();

        return redirect('/')->with(['success' => 'Подписка активирована']);

    }
}
