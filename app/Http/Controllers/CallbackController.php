<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 14.04.2017
 * Time: 13:28
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use App\Events\CallBackRequestEvent;
use App\Model\Callback;
use App\Http\Requests\StoreCallback;

class CallbackController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function callback(StoreCallback $request)
    {
        $data = $request->all();
        unset($data['robot']);
        unset($data['terms']);
        $callback = Callback::create($data);
        $callback->save();
        event(new CallBackRequestEvent($callback));
        return response()->json('Ваша заявка принята! В ближайшее время наши консультанты с Вами свяжутся.');
    }


}