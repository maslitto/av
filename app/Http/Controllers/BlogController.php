<?php

namespace App\Http\Controllers;

use App\Model\Blog;
use App\Model\Page;
use Illuminate\Http\Request;

/**
 *
 * Class BlogController
 * @package App\Http\Controllers
 */
class BlogController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page = Page::where('title', 'Блог')->firstOrFail();
    	$blogs = Blog::where('published','>',0)->orderBy('created_at','desc')->paginate(12);
        return view('blog.blog',[
            'blogs' => $blogs,
            'page' => $page,
        ]);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($slug)
    {
        $blog = Blog::where('slug', $slug)->firstOrFail();
        return view('.blog.blog-post',[
            'page' => $blog,
            'post' => $blog,
        ]);
    }

}
