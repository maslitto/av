<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Model\User;
use App\Model\Order;
use App\Events\OrderStored;
use App\Events\AutoRegisterUserEvent;

class OrderController extends Controller
{
    public function step3(Request $request)
    {
        //
        $array = $request->all();
        unset($array['q']);
        foreach($array as $k => $v){
            session(['order.'.$k => $v]);
        }
        session(['step'=>3]);
        return view('order');
    }

    public function step4(Request $request)
    {
        $array = $request->all();
        unset($array['q']);
        foreach($array as $k=>$v){
            session(['order.'.$k => $v]);
        }
        session(['step'=>4]);
        return view('order');
    }

    public function finish(Request $request)
    {
        $array = $request->all();
        unset($array['q']);
        foreach($array as $k => $v){
            session(['order.'.$k => $v]);
        }
        session(['step'=>5]);
        if(session()->has('order.q'))
            session()->forget('order.q');
        $data = session()->get('order');
        $user = User::where('email', '=', $data['email'])->first();

        if ($user === null) {
            //if user doesn't exist create new one
            $user = new User();
            $password = str_random(6);
            $user->email = $data['email'];
            $user->name = $data['name'];
            $user->password = $password;
            $user->second_name = $data['second_name'];
            $user->index = $data['index'];
            $user->country = $data['country'];
            $user->city = $data['city'];
            $user->street = $data['street'];
            $user->home = $data['home'];
            $user->apt = $data['apt'];
            $user->phone = $data['phone'];
            $user->active = 1;
            $user->save();
            event(new AutoRegisterUserEvent($user,$password));
        }
        unset($data['_token']);
        $data['status'] = 0;
        $data['user_id'] = $user->id;
        $data['order_cost'] = Cart::subtotal(0,'','');

        $order = Order::create($data);
        $order->save();

        event(new OrderStored($order,Cart::content()));

        return view('order',[
            'order' => $order
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        session(['step'=>2]);
        return view('order',[]);
    }

}
