<?php
namespace App\Http\Controllers;
 
use App\Http\Requests;
use Image;
use Storage;
use Response;
use Illuminate\Http\Request;
use Validator;
class ImageController extends Controller
{
    /**
     * livetime image in cache in min (default 129600 ~3 month )
     * @var int
     */
    protected $cacheTime=40000; // about month
 
    public function __construct()
    {
        $this->middleware('web');
    }
 
    /**
     * @param $dateImg
     * @param $filename
     * @param $w
     * @param $h
     * @param null $type
     * @param null $anchor possible: top-left, top, top-right, left, center (default), right, bottom-left, bottom, bottom-right
     * @return mixed
     */
    public function whResize($dateImg, $filename, $w , $h , $type=null, $anchor=null)
    {
        $filePath = public_path('images/' . $dateImg .'/'. $filename);
        if (! $anchor) $anchor='center';
        if (! $type) $type='outbox';
        if ($w=='w') $w=null;
        if ($h=='h') $h=null;
 
        $params = (object) array(
            'filePath' =>$filePath,
            'w' => $w,
            'h' => $h,
            'cw' => $w,
            'ch' => $h,
            'anchor' => $anchor,
        );
 
        switch ($type) {
            case 'asis':
                $cacheImage = Image::cache(function($image) use( $filePath, $w, $h, $type){
                    return $image->make($filePath)->resize($w, $h);
                });
                break;
            case 'prop':
                if($w>$h) $params->w = null;
                else $params->h = null;
                $cacheImage = $this->resizeAndChunk($params);
                break;
            case 'chunk':
                if($w>$h) $params->h = null;
                else $params->w = null;
                $cacheImage = $this->resizeAndChunk($params);
                break;
            case 'fit':
                $cacheImage = Image::cache(function($image) use( $filePath, $w, $h, $type){
                    return $image->make($filePath)->fit($w, $h);
                }, 10, false);
                break;    
            case 'outbox':
                $cacheImage = Image::cache(function($image) use( $params){
                    return $image->make($params->filePath)->resize($params->w, $params->h, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                },$this->cacheTime,false);
                break;
        }
        return Response::make($cacheImage, 200, array('Content-Type' => 'image/jpeg'));
    }

    /**
     * @param $params
     * @return Image
     */
    protected function resizeAndChunk($params)
    {
        return Image::cache(function($image) use( $params){
                return $image->make($params->filePath)->resize($params->w, $params->h, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->resizeCanvas($params->cw, $params->ch, $params->anchor);
        },$this->cacheTime,false);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function upload(Request $request){
        $defaultUploadPath = 'images/avatars';
        //get files from request
        $file = $request->file('file');
        $validator = Validator::make($request->all(), [
            'file' => 'image',
            
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation error',
                'errors' => $validator->errors()->get('file'),
            ], 400);
        }
        $extension = $request->file('file')->getClientOriginalExtension();
        //set unique filename
        $filename = uniqid().'.'.$extension;


        $file->move($defaultUploadPath, $filename);
        $path = $defaultUploadPath.'/'.$filename;
        return response()->json(['url' => asset($path), 'value' => $path]);
    }
 
}