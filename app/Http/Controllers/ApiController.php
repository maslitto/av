<?php

namespace App\Http\Controllers;

use App\Model\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 *
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{

    /**
     */
    public function getUsers(Request $request)
    {
        if($request->headers->get('key') !== env('API_KEY')){
            return new Response('Неверный ключ API',Response::HTTP_UNAUTHORIZED);
        } else {
            $data = $request->request->get('users');
            file_put_contents(storage__path().'/sync1c/users.txt',print_r($data,true));
            return new Response('Data received',Response::HTTP_OK);
        }
    }


    public function getOrders(Request $request)
    {
        /*if($request->headers->get('key') == env('API_KEY')){
            return new Response('Неверный ключ API',Response::HTTP_UNAUTHORIZED);
        } else {*/
            $date = $request->request->get('from-time');
            $orders = Order::with('products.product')->where('created_at','>=',$date)->get();

            return response()->json($orders);
        /*}*/
    }

}
