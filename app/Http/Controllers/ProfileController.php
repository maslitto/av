<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Order;
use App\Model\Product;
use App\Http\Requests\ChangePassword;

/**
 * Class for user's profile
 * Class ProfileController
 * @package App\Http\Controllers
 */

class ProfileController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = auth()->user();
        $orders = Order::where('user_id',$user->id)->get();
        $wishlist = [];
        if(session()->has('wishlist')){
            $wishlist = Product::whereIn('id',session()->get('wishlist'))->get();
        }

        return view('profile',[
            'user' => $user,
            'orders' => $orders,
            'wishlist' => $wishlist
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $user->update($request->all());
        $user->save();
        return back()->with('status','Профиль обновлен');
    }

    /**
     * @param ChangePassword $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function changePassword(ChangePassword $request)
    {
        $user = auth()->user();
        $user->password = $request->password;
        $user->save();
        return back()->with('status','Пароль успешно изменен');


    }
}
