<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Model\ConfirmUser;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    /**
     * Email sender
     *
     * @var string
     */
    protected $from;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->from = config('app.manager_mail');
        $this->middleware('guest');
    }
    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        //dd($request->all());
        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);
        $this->sendToken($request->all());
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    protected function validator(array $data)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'robot'=>'in:fuck-u-robots',
            'terms'=>'accepted'
        ];

        $messages = [
            'name.required'        => 'Укажите имя',
            'email.required'        => 'Укажите e-mail',
            'email.unique'        => 'Такой e-mail уже зарегистрирован',
            'email.email'        => 'Укажите корректный e-mail ',
            'password.required'        => 'Укажите пароль',
            'password.confirmed'        => 'Введенные пароли не совпадают',
            'password.min'        => 'Пароль должен быть минимум из 6 знаков',
            'terms.accepted'        => 'Подтвердите согласие с правилами обработки данных',
        ];
        $validation = Validator::make($data, $rules, $messages);

        return $validation;

    }
    /**
     * Send confirmation token.
     *
     * @param  array  $data
     * @return void
     */
    protected function sendToken(array $data)
    {
        $token = str_random(32);
        $confirmUser = new ConfirmUser;
        $confirmUser->token = $token;
        $confirmUser->email = $data['email'];
        $confirmUser->save();
        //отправляем ссылку с токеном пользователю
        Mail::send('auth.emails.confirm',['token'=>$token],function($u) use ($data){
            $u->from(config('app.manager_mail'), config('app.name'));
            $u->to($data['email'],$data['name']);
            $u->subject('Подтверждение регистрации на сайте angelo-vani.ru');
        });

    }
    /**
     * Confirm registration.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function confirm($token){
        $model = ConfirmUser::where('token','=',$token)->firstOrFail(); //выбираем запись с переданным токеном, если такого нет то будет ошибка 404
        $user = User::where('email','=',$model->email)->first(); //выбираем пользователя почта которого соответствует переданному токену
        $user->active = 1; // меняем статус на 1
        $user->save();  // сохраняем изменения
        $model->delete(); //Удаляем запись из confirm_users
        $this->guard()->login($user);
        return redirect('/');

    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'opt' => (int) $data['opt'],
            'subscribe' =>(int) $data['subscribe'],
            'active' => 0
        ]);
    }

    /**
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    protected function registered(Request $request, $user) {
        return response()->json(['message' => 'Успешная регистрация. На вашу почту вылано письмо с подтверждением регистрации']);
    }
}
