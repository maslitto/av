<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\Page;
use App\Model\Tag;
use Illuminate\Http\Request;
use App\Services\ProductService;

class CatalogController extends Controller
{
    private $productService;

    public function __construct()
    {
        $this->productService = new ProductService();
    }

    public function search(Request $request)
    {
        $q = $request->q;
        //if ajax request
        if(($request->ajax()) && (strlen($q)>2)){
            $products = $this->productService->search($q);
            return view('search/search-result',['products' => $products]);
        }

        $products = $this->productService->search($q);
        return view('search.search',[
            'products' => $products,
            'page' => Page::find(10),
            'q' => $q
        ]);

    }

    /**
     * Catalog page index
     * @param Request $request
     * @param null $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(Request $request, $slug = null)
    {
        $routeName = \Request::route()->getName();
        if(isset($slug)){
            if($routeName == 'catalog'){
                $page = Page::where('slug',$slug)->first();
            }
            if($routeName == 'tag'){
                $page = Tag::where('slug',$slug)->first();
            }
        } else {
            $page = Page::where('id', 2)->first();
        }
        $productsQuery = $this->productService->getProductsQuery($request->all(),$page);

        $lastviews = $this->productService->getLastViews();
        //$tags1 = Tag::where('parent_id',1)->get();
        $tags1 = Tag::where('parent_id',1)->active()->get();
        $tags2 = Tag::where('parent_id',2)->active()->get();
        //$tags2 = Tag::where('parent_id',2)->get();
        if($routeName == 'tag'){
            $slug = $page->slug;
            if($page->parent_id == 1){
                $tags1 = Tag::where('parent_id',1)->active()->get();
                $tags2 = Tag::where('parent_id',3)->active()->where('slug','LIKE',"%$slug%")->get();
            }
            if($page->parent_id == 2){
                $tags1 = Tag::where('parent_id',2)->active()->get();
                $tags2 = Tag::where('parent_id',3)->active()->where('slug','LIKE',"%$slug%")->get();
            }
            if($page->parent_id == 3){
                $tags1 = Tag::where('parent_id',1)->active()->get();
                $tags2 = Tag::where('parent_id',2)->active()->get();
            }
        }
        //dd($productsQuery->paginate(Product::NUM_ITEMS));

        $response = [
            'products' => $productsQuery->paginate(Product::NUM_ITEMS),
            'total' => $productsQuery->count(),
            'page' => $page,
            'tags1' => $tags1,
            'tags2' => $tags2,
            'lastviews' => $lastviews,
            'filters' => $this->productService->getFilters($page)
        ];
        if($request->ajax()){
            return response()->json(['total' => count($productsQuery->get())]);
        }
        return view('catalog.catalog',$response);

    }

    /**
     * Shows single product
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        $next = $this->productService->getNextProduct($product->id);
        $prev = $this->productService->getPrevProduct($product->id);
        $lastviews = $this->productService->getLastViews();
        $this->productService->pushToLastviews($product);
        $recommends = Product::active()->where('collection',$product->collection)->take(25)->get()->shuffle();
        return view('product.details',[
            'product' => $product,
            'recommends' => $recommends,
            'lastviews' => $lastviews,
            'page' => $product,
            'next' => $next,
            'prev' => $prev
        ]);
    }

    /**
     * Get product by id for fast watch in modal window
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProductById(Request $request)
    {
        $product = Product::where('id', $request->pid)->firstOrFail();
        $next = $this->productService->getNextProduct($product->id);
        $prev = $this->productService->getPrevProduct($product->id);
        $this->productService->pushToLastviews($product);
        $product = Product::where('id', $request->pid)->first();
        return view('product.modalproduct',[
            'product' => $product,
            'next' => $next,
            'prev' => $prev
        ]);
    }
}
