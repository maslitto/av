<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Product;
use Cart;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wishlist = [];
        if(session()->has('wishlist'))
            $wishlist = Product::whereIn('id',session()->get('wishlist'))->get();
        return view('cart.wishlist',[
            'wishlist' => $wishlist,
        ]) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pid = $request->pid;
        if(session()->has('wishlist')){
            $ids = session()->get('wishlist');

            if(!in_array($pid,$ids)){
                session()->push('wishlist', $pid);
            } else{
                $wishlist = array_diff($ids, array($pid));
                session(['wishlist' => $wishlist]);
            }
        } else{
            session()->push('wishlist', $pid);
        }
        session(['wishlist' => array_diff(session()->get('wishlist'), array(NULL))]);
        if(count(session()->get('wishlist'))<1){
            session()->forget('wishlist');
        }

        $response = [
            'success' => true,
            'message' => 'Товар добавлен в избранное',
            'count' => count(session()->get('wishlist')),
        ];
        return response()->json($response);
    }

    /**
     * Push all products from wishlist to cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function buyAll(Request $request)
    {
        //
        $products = Product::whereIN('id', session()->get('wishlist'))->get();
        foreach($products as $product){
            $cartItem = Cart::add($product->id, $product->title, 1, $product->price,[]);
            Cart::associate($cartItem->rowId, 'App\Model\Product');
        }
        session()->forget('wishlist');
        $response = [
            'success' => true,
            'message' => 'Товар добавлен в корзину',
            'count' => Cart::count(),
            'wcount' => count(session()->get('wishlist')),
            'content' => view('cart.basket-items',['items' => Cart::content()])->render(),
        ];
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        session()->forget('wishlist');
        $response = [
            'success' => true,
            'message' => 'Избранное очищено',
            'count' => count(session()->get('wishlist')),
        ];
        return response()->json($response);
    }

}
