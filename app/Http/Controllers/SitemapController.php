<?php

namespace App\Http\Controllers;
use App\Model\Page;
use App\Model\Tag;
use App\Model\Product;

class SitemapController extends Controller
{
    public function sitemap()
    {
        $products = Product::active()->get();
        $pages = Page::all();
        $tags = Tag::active()->get();
        /*return view('sitemap',[
            'pages' => $pages,
            'products' => $products
        ]);*/
        return response()
            ->view('sitemap',[
                'pages' => $pages,
                'products' => $products,
                'tags' => $tags,
                'host' => env('APP_URL')
            ])
            ->header('Content-Type', 'text/xml');
    }

}
