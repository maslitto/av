<?php

namespace App\Http\Controllers;
use App\Model\Banner;
use App\Model\Page;
use App\Model\Blog;
use App\Services\ProductService;

class HomeController extends Controller
{
    /**
     * @var ProductService
     */
    private $productService;

    /**
     * HomeController constructor.
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Shows the shop's homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();
        $sales = $this->productService->getSaleProducts(15);
        $new = $this->productService->getNewProducts(15);
        $page = Page::find(1);
        $news = Blog::where('published','>',0)->orderBy('created_at','desc')->limit(10)->get();
        return view('home.home',[
            'page' => $page,
        	'banners' => $banners,
            'populars' => $new,
            'sales' => $sales, 
            'news' => $news
    	]);
    }
}
