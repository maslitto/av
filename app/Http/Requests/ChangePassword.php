<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;
use Hash;

class changePassword extends FormRequest
{
    /**
     * changePassword constructor.
     */
    public function __construct(ValidationFactory $validationFactory)
    {
        parent::__construct();
        $validationFactory->extend(
            'old_password',
            function ($attribute, $value, $parameters) {
                return Hash::check($value, current($parameters));
            }
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth()->user();
        if(isset($user)){
            return true;
        } else{
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'old_password' => 'required|old_password:' . auth()->user()->password,
            'password' => 'required|confirmed',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'old_password.required'        => 'Укажите старый пароль',
            'old_password.old_password'        => 'Не верно указан старый пароль',
            'password.required'        => 'Введите новый пароль',
            'password.confirmed'        => 'Введенные пароли не совпадают',
        ];
    }
}
