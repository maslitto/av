<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Product;
use App\Model\OrderProduct;
use App\Model\Page;

class UpdateProducts extends Command
{
    /**
     * Json file path
     * @var string
     */
    private $json;
    /**
     * Images folder path
     * @var string
     */
    private $photos;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновление товаров из выгрузки 1с';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->json  = 'ftpuser/nom.json';
        $this->photos = 'ftpuser/Card/';
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->parse();
    }
    /**
     * Deletes files in directory $str
     * @param string $str
     * @return bool
     */
    public function deleteFiles(string $str) {
        if (is_file($str)) {
            return unlink($str);
        } elseif (is_dir($str)) {
            $scan = glob(rtrim($str,'/').'/*');
            foreach($scan as $index=>$path) {
                $this->deleteFiles($path);
            }
            return @rmdir($str);
        }
    }

    public function parse()
    {
        ini_set('max_execution_time', 3600);
        $this->deleteOldProducts();
        $string = file_get_contents(public_path($this->json));
        $products = json_decode($string, false,5);
        $count = 0;
        foreach($products as $k => $product){
            if(empty($product->collection)||empty($product->Card)||empty($product->gender||$product->Card=='')) continue;

            $exist = Product::where('guid',$product->GUID)->first();
            if(!$exist){
                $new = new Product();
            } else{
                $new = $exist;
            }
            $new->guid = $product->GUID;
            $new->title = $product->FullName;
            $new->sku = $product->sku;
            $new->card = $product->Card;
            $new->vid = $product->vid;
            $new->gender = $product->gender;
            if($product->gender == 'Женская'){
                $category = Page::where('title','Женские сумки')->first();
            } elseif($product->gender == 'Мужская'){
                $category = Page::where('title','Мужские сумки')->first();
            }

            $new->collection = $product->collection;
            if(($product->sale == 'Да')||(!empty($product->PriceSale))) $product->collection = 'Распродажа';
            $collection = Page::where('title',$product->collection)->where('parent_id',$category->id)->first();
            if(!$collection){
                $collection = new Page();
                $collection->title = $product->collection;
                $collection->save();
                $collection->makeChildOf($category);
            }
            $new->season = $product->season;
            $new->vidnomenklature = $product->VidNomenklature;

            $new->material = $product->material;
            $new->width = $product->width;
            $new->height = $product->height;
            $new->length = $product->length;
            $new->color = $product->color;
            $new->description = $product->description;
            $product->new == 'Да' ? $new->new = 1 : $new->new = 0;
            $product->hit == 'Да' ? $new->hit = 1 : $new->hit = 0;
            $product->req == 'Да' ? $new->req = 1 : $new->req = 0;
            $product->sale == 'Да' ? $new->sale = 1 : $new->sale = 0;
            $new->stock = $product->count;

            $new->price_sale = preg_replace('/[^0-9]/', '', $product->PriceSale);
            if($product->PriceSale>0){
                $new->price_opt = preg_replace('/[^0-9]/', '', $product->PriceSale);
                $new->price = preg_replace('/[^0-9]/', '', $product->PriceSale);
                $new->old_price = preg_replace('/[^0-9]/', '', $product->PriceRoznica);
            }  else{
                $new->price_opt = preg_replace('/[^0-9]/', '', $product->PriceOpt);
                $new->price = preg_replace('/[^0-9]/', '', $product->PriceRoznica);
            }

            $new->category_id = $collection->id;
            $new->slug = '';
            $new->save();
            $count++;
            $this->updateImages();
        }

    }

    private function updateImages()
    {
        set_time_limit(0);
        $path = public_path($this->photos);

        $images = glob($path . "*.jpg");
        foreach($images as $image){
            $info = pathinfo($image);
            if(is_array($info)){
                $filename = $info['filename'];
                if($filename){
                    $path = $this->photos . $filename . '.' .$info['extension'];
                    $product = Product::where('guid',$filename)->first();
                    if($product){
                        $product->images = array($path);
                        $product->save();
                    }
                }
            }
        }
    }

    private function deleteOldProducts()
    {
        $olds = Product::all();
        foreach($olds as $old){
            $order = OrderProduct::where('product_id',$old->id)->first();
            if(!$order){
                $old->delete();
            } else{
                $old->show = 0;
                $old->save();
            }
        }
    }
}
